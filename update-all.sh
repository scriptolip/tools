#!/bin/bash

#: Title       : Pepper&Carrot Installer and Updater ; install.sh
#: Author      : David REVOY < info@davidrevoy.com >, Mjtalkiewicz (aka Player_2)
#: License     : GPL

# a Bash script to update all the source of Pepper&Carrot

# Information
export script_title="Update all"
export script_version="2.0a"
export script_filename="update-all.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config
function_dependency_check zip git wget lftp inkscape

# Script
function_decorative_header

# Carrot Ascii-art badge
echo " ${Yellow}${PurpleBG}                        ${Off}"
echo " ${Yellow}${PurpleBG}       /|_____|\        ${Off}"
echo " ${Yellow}${PurpleBG}      /  ' ' '  \       ${Off}"
echo " ${Yellow}${PurpleBG}     < ( .  . )  >      ${Off}"
echo " ${Yellow}${PurpleBG}      <   '◡    >       ${Off}"
echo " ${Yellow}${PurpleBG}        '''|  \         ${Off}"
echo " ${Yellow}${PurpleBG}                        ${Off}"

function_decorative_title "Update repos"
# ======================================
  
  # webcomics
  function_git_update "$project_root"/"$folder_webcomics"
  
  # wiki
  function_git_update "$project_root"/"$folder_wiki"
  
  # documentation
  function_git_update "$project_root"/"$folder_doc"
  
  # scenarios
  function_git_update "$project_root"/"$folder_scenarios"
  
  # website (commented; I prefer to update the git of website manually at the moment)
  # function_git_update "$project_root"/"$folder_website"
  
  # tools
  function_git_update "$project_root"/"$folder_tools"
  
  # fonts
  function_git_update "$project_root"/"$folder_fonts"
  
function_decorative_title "Grab external data"
# ======================================

  # grab json from https://wwww.davidrevoy.com
  echo " * update comments.json"
  wget -O "$project_root"/"$folder_webcomics"/comments.json https://www.davidrevoy.com/static7/listing

function_decorative_title "Upload changes for Wiki and documentation"
# ======================================

  # wiki
  function_ftp_upload "$project_root"/"$folder_wiki" "/www/data/wiki/"
  
  # documentation
  function_ftp_upload "$project_root"/"$folder_doc" "/www/data/documentation/"
  
  # scenarios
  function_ftp_upload "$project_root"/"$folder_scenarios" "/www/data/scenarios/"

function_decorative_title "Webcomics"
# ======================================

  cd "$project_root"/"$folder_webcomics"
  # Loop all episodes
  for folder_episode in *ep[0-9][0-9]*; do
    echo "folder: $folder_episode" | tee -a "$log_file"
    
    # Generate zip for lang and art and use that to guess if the folder need to be sent to renderfarm.
    export path_episode="$project_root"/"$folder_webcomics"/"$folder_episode"
    export tokken_folderneedrendering=0
    export file_langzip="$folder_episode""_lang-pack.zip"
    export file_artzip="$folder_episode""_art-pack.zip"
    export file_artmd5=$(echo $file_artzip|sed 's/\(.*\)\..\+/\1/')".md5"
    export file_langmd5=$(echo $file_langzip|sed 's/\(.*\)\..\+/\1/')".md5"
    export file_artmd5tmp=$(echo $file_artzip|sed 's/\(.*\)\..\+/\1/')"_tmp.md5"
    export file_langmd5tmp=$(echo $file_langzip|sed 's/\(.*\)\..\+/\1/')"_tmp.md5"
    
    # only episode folders
    if [ ! -d "$path_episode" ]; then
      break
    fi

    cd "$path_episode"

    # check if the folder we needs exists
    if [ ! -d "$path_episode"/"$folder_zip" ]; then
      mkdir -p "$path_episode"/"$folder_zip"
      echo "+  mkdir $folder_zip" | tee -a "$log_file"
    fi
    if [ ! -d "$path_episode"/"$folder_cache" ]; then
      mkdir -p "$path_episode"/"$folder_cache"
      echo "+  mkdir $folder_cache" | tee -a "$log_file"
    fi
    
    # reset our tmp files
    rm -f "$path_episode"/"$folder_cache"/"$file_artmd5tmp"
    rm -f "$path_episode"/"$folder_cache"/"$file_langmd5tmp"

    # Art-pack
    if [ ! -f "$path_episode"/"$folder_cache"/"$file_artmd5" ]; then
      touch "$path_episode"/"$folder_cache"/"$file_artmd5"
    fi
    cd "$path_episode"/

      # Generate on-the-fly a full checksum list of our art sources pattern
      find -maxdepth 1 \( -name "*.kra" -o -name "*.png" -o -name "*.gif" \) -type f -exec md5sum {} \; | cut -d" " -f1 | sort >> "$path_episode"/"$folder_cache"/"$file_artmd5tmp"
      # Compare if actual *.kra checksum is similar to the previous one recorded on txtfile
      if cmp -s "$path_episode"/"$folder_cache"/"$file_artmd5tmp" "$path_episode"/"$folder_cache"/"$file_artmd5"; then
        echo "[art]  up-to-date." | tee -a "$log_file"
      else
        echo "x [art]  modified" | tee -a "$log_file"
        export tokken_folderneedrendering=1
        # We accept a new version as default
        zip --quiet "$file_artzip" *.kra *.png *.gif | tee -a "$log_file"
        mv "$file_artzip" "$folder_zip"/"$file_artzip"
        echo "  => Updated $file_artzip" | tee -a "$log_file"
        rm -f "$path_episode"/"$folder_cache"/"$file_artmd5"
        find -maxdepth 1 \( -name "*.kra" -o -name "*.png" -o -name "*.gif" \) -type f -exec md5sum {} \; | cut -d" " -f1 | sort >> "$path_episode"/"$folder_cache"/"$file_artmd5"
        echo "  => Updated md5sum in cache" | tee -a "$log_file"
      fi
      # Clean the tmp file
      rm -f "$path_episode"/"$folder_cache"/"$file_artmd5tmp"
      
    # Lang-pack
    if [ ! -f "$path_episode"/"$folder_cache"/"$file_langmd5" ]; then
      touch "$path_episode"/"$folder_cache"/"$file_langmd5"
    fi
    cd "$path_episode"/

      # Generate on-the-fly a full checksum list of our lang folder
      find lang/ -type f -exec md5sum {} \; | cut -d" " -f1 | sort >> "$path_episode"/"$folder_cache"/"$file_langmd5tmp"
      # Compare if actual *.kra checksum is similar to the previous one recorded on txtfile
      if cmp -s "$path_episode"/"$folder_cache"/"$file_langmd5tmp" "$path_episode"/"$folder_cache"/"$file_langmd5"; then
        echo "[lang] up-to-date." | tee -a "$log_file"
      else
        echo "x [lang] modified" | tee -a "$log_file"
        export tokken_folderneedrendering=1
        # We accept the new version as default
        zip --quiet -r "$file_langzip" lang/ | tee -a "$log_file"
        mv "$file_langzip" "$folder_zip"/"$file_langzip"
        echo "  => Updated $file_langzip" | tee -a "$log_file"
        rm -f "$path_episode"/"$folder_cache"/"$file_langmd5"
        find lang/ -type f -exec md5sum {} \; | cut -d" " -f1 | sort >> "$path_episode"/"$folder_cache"/"$file_langmd5"
        echo "  => Updated md5sum in cache" | tee -a "$log_file"
      fi
      # Clean the tmp file
      rm -f "$path_episode"/"$folder_cache"/"$file_langmd5tmp"


    # Error checking
    if [ ! -f "$path_episode"/"$folder_zip"/"$file_langzip" ]; then
      echo "x Error: $file_langzip is missing" | tee -a "$log_file"
    fi
    if [ ! -f "$path_episode"/"$folder_zip"/"$file_artzip" ]; then
      echo "x Error: $file_artzip is missing" | tee -a "$log_file"
    fi
    
    # Launch renderfarm if a change was detected
    if [ "$tokken_folderneedrendering" = 1 ]; then
       "$project_root"/"$folder_tools"/renderfarm.sh "$project_root"/"$folder_webcomics"/"$folder_episode"

        # Color any episodes*.json generation errors in red
        echo "$Red"

        # Generate episodes.json
        if "$project_root/$folder_tools/utils/generate-json-episodes-list.py" \
           "$project_root/$folder_webcomics" \
           > "$project_root/$folder_webcomics/episodes.json.tmp"
        then
          mv "$project_root/$folder_webcomics/episodes.json.tmp" \
             "$project_root/$folder_webcomics/episodes.json"
        fi

        # Generate episodes-v1.json
        if "$project_root/$folder_tools/utils/generate-json-episodes-v1-list.py" \
           "$project_root/$folder_webcomics" \
           > "$project_root/$folder_webcomics/episodes-v1.json.tmp"
        then
          mv "$project_root/$folder_webcomics/episodes-v1.json.tmp" \
             "$project_root/$folder_webcomics/episodes-v1.json"
        fi

        # Back to normal color
        echo "$Off"

    fi
    
  done

function_decorative_title "Fonts: generate zip"
# =============================================
export fontziplatest="peppercarrot-fonts-latest.zip"
export fontmd5sumtmp="md5sum_tmp.txt"
export fontmd5sum="md5sum.txt"
export fontsha256sum="sha256sum.txt"

  # Check if the infra-structure exist
  if [ -d "$project_root"/"$folder_webcomics"/0ther/tools/zip/cache/ ]; then
    echo " * Font folder found" 
  else
    echo "${Green} * creating font folder missing ${Off}"
    mkdir -p "$project_root"/"$folder_webcomics"/0ther/tools/zip/cache
  fi

  # Generate on-the-fly a full checksum list of our lang folder
  cd "$project_root"/
  find "$folder_fonts"/ -type f -exec md5sum {} \; | cut -d" " -f1 | sort >> "$project_root"/"$folder_webcomics"/0ther/tools/zip/cache/"$fontmd5sumtmp"
  # Compare if actual md5checksum is similar to the previous one recorded
  if cmp -s "$project_root"/"$folder_webcomics"/0ther/tools/zip/cache/"$fontmd5sum" "$project_root"/"$folder_webcomics"/0ther/tools/zip/cache/"$fontmd5sumtmp"; then
    echo "[font] up-to-date." | tee -a "$log_file"
  else
    echo "x [font] modified" | tee -a "$log_file"

    zip --quiet -r "$fontziplatest" "$folder_fonts"/ | tee -a "$log_file"
    mv "$fontziplatest" "$project_root"/"$folder_webcomics"/0ther/tools/zip/
    echo "  => Updated $fontziplatest" | tee -a "$log_file"
    rm -f "$project_root"/"$folder_webcomics"/0ther/tools/zip/cache/"$fontmd5sum"
    
    find "$folder_fonts"/ -type f -exec md5sum {} \; | cut -d" " -f1 | sort >> "$project_root"/"$folder_webcomics"/0ther/tools/zip/cache/"$fontmd5sum"
    echo "  => Updated md5sum in cache" | tee -a "$log_file"
    
    #Regenerate public checksum
    cd "$project_root"/"$folder_webcomics"/0ther/tools/zip/
    rm md5sum.txt
    rm sha256sum.txt
    find -maxdepth 1 \( -name "*.zip" \) -type f -exec md5sum {} \; | sed 's/.\///g' | sort >> "$fontmd5sum"
    find -maxdepth 1 \( -name "*.zip" \) -type f -exec sha256sum {} \; | sed 's/.\///g' | sort >> "$fontsha256sum"
    echo "  => Updated public $fontmd5sum and $fontsha256sum " | tee -a "$log_file"
  fi
  # Clean the tmp file
  rm -f "$project_root"/"$folder_webcomics"/0ther/tools/zip/cache/"$fontmd5sumtmp"
  cd "$project_root"/"$folder_webcomics"/

function_decorative_title "Regenerate last_updated.txt"
# =============================================
# Notify website's cache of an update, inject Unix time and human friendly
date +'%s' > "$project_root"/"$folder_webcomics"/last_updated.txt
date +'%d/%m/%Y%t%H:%M:%S' >> "$project_root"/"$folder_webcomics"/last_updated.txt
cat "$project_root"/"$folder_webcomics"/last_updated.txt

# Footer library
function_load_source_lib "$script_absolute_path"/lib/footer.sh

# Auto upload changes
"$project_root"/"$folder_tools"/uploader.sh

printf "\033]0;%s\007\n" "Renderfarm (done)"

if [ "$1" = "--prompt" ]; then
# Task is executed inside a terminal window (pop-up)
# This line prevent terminal windows to be auto-closed
# and necessary to read log later, or to reload the script
 echo "${Purple}  Press [Enter] to launch renderfarm again or:"
 echo "${White}${BlueBG}[q]${Off} => to Quit. ${Off}"
 echo "${White}${BlueBG}[u]${Off} => to launch an Upload only. ${Off}"
 read -p "?" ANSWER
 if [ "$ANSWER" = "q" ]; then
   exit
 elif [ "$ANSWER" = "u" ]; then
  # Launch a new upload without prompting at the end
  "$project_root"/"$folder_tools"/uploader.sh
 else
  # Launch this very script again
   "$project_root"/"$folder_tools"/"$script_filename" --prompt
 fi
fi
