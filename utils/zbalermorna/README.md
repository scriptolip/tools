# Usage

This tool was designed to generate jz translation from jb.

author: Gleki

# Install

* install lakmeer's zbalermorna font globally
* clone webcomics repo next to tools repo
* run `node piper.js` from `/tools/zbalermorna` directory
* in each episode open `/lang/jz` folder, correct `*.svg` files, especially font size, baloon sizes and fix any text, not transpiled into zbalermorna. Use `\lang/jb` folder as a reference.
* Create a merge request 

# Environment structure

- /webcomics/
    - ep01_Potion-of-Flight/
        - lang/
            - jb/
            - jz/
- /tools/
    - utils
        - zbalermorna/
            - piper.js
