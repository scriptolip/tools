#!/usr/bin/env python3

help_text = """Usage: renderfarm-lite.py [LANGUAGE]...
Render Pepper&Carrot translations

This renderfarm lite only renders translations. It's perfect for translators or translation reviewers who wish to render a whole episode without the fuss of Krita source files and the likes.

How to use:
* Render all languages:
    $ cd comics/ep24    or    cd comics/ep24/lang
    $ ~/pc/tools/utils/renderfarm-lite.py

* Render English and French:
    $ cd comics/ep24    or    cd comics/ep24/lang
    $ ~/pc/tools/utils/renderfarm-lite.py en fr

* Render English:
    - either in the same way as above, or
    - $ cd comics/ep24/lang/en
      $ ~/pc/tools/utils/renderfarm-lite.py

You can a install symlink to the script in a directory in your PATH to be able to call it without giving the full path, but that is outside the scope of this help text.

Original author: Midgard
License: GPLv3 or later
"""

import sys
import subprocess
import os
from os import path
from glob import glob

# Config
inkscape_command = "inkscape"


# Rendering

def render_lang(lang, source_dir, dest_dir):
    """
    :param lang: language abbreviation, only for logging
    :param source_dir: path to directory with SVG files to render
    :param dest_dir: path to directory to put rendered images in
    """
    print("Rendering [{}]".format(lang))
    for filename in sorted(os.listdir(source_dir)):
        if path.splitext(filename)[1] == ".svg":
            dest_filename = other_extension(filename, ".png")
            dest_file   = path.join(dest_dir,   dest_filename)
            source_file = path.join(source_dir, filename)

            if not path.isfile(dest_file) \
                or path.getmtime(source_file) > path.getmtime(dest_file):

                print(" → {}".format(filename))
                subprocess.run(
                    (
                        inkscape_command,
                        "-e",
                        dest_file,
                        source_file
                    ),
                    check=True
                )
            else:
                print(" → {} is up-to-date".format(dest_filename))


# File system and paths stuff

def other_extension(filename, ext):
    """
    Build a filename with the same base but another extension.

    :param ext: new extension, including the dot
    """
    return path.splitext(filename)[0] + ext


def file_count(directory, pattern):
    return len(glob(
        path.join(directory, pattern)
    ))


def is_valid_lang_name(lang):
    return (
        2 <= len(lang) <= 3 and
        lang.isalpha() and
        lang.islower()
    )


def is_valid_lang_dir(lang, lang_path):
    return (
        is_valid_lang_name(lang) and
        path.isdir(lang_path) and
        file_count(lang_path, pattern="*.svg") >= 3
    )


# Main

def subdir_if_necessary(directory):
    if (path.basename(directory) != "lang" and
        path.isdir(path.join(directory, "lang"))
    ):
        return path.join(directory, "lang")

    else:
        return directory


def main():
    try:
        if sys.argv[1] == "-h" or sys.argv[1] == "--help":
            print(help_text, file=sys.stderr)
            exit(0)
    except IndexError:
        pass

    complain_invalid_langs = False

    directory = os.getcwd()

    # This if-else cascade is not particularly elegant, but I wanted
    # the script to detect several possible CWDs.

    # If there are arguments, always prefer those.
    if len(sys.argv) > 1:
        directory = subdir_if_necessary(directory)
        langs     = sys.argv[1:]
        complain_invalid_langs = True

    # If there are ≥3 SVGs in the CWD, assume we are in a language
    # directory and render it.
    elif file_count(directory, pattern="*.svg") >= 3:
        langs     = (path.basename(directory),)
        directory =  path.dirname (directory)

    # If cwd isn't called "lang" but there's a subdir "lang", start
    # from that subdir.
    else:
        directory = subdir_if_necessary(directory)
        langs     = os.listdir(directory)


    destination_dir = path.join(directory, "../render")


    count_langs_done = 0

    for lang in langs:
        lang_path = path.join(directory,       lang)
        lang_dest = path.join(destination_dir, lang)

        if is_valid_lang_dir(lang, lang_path):
            os.makedirs(lang_dest, exist_ok=True)

            render_lang(lang, lang_path, lang_dest)
            count_langs_done += 1

        elif complain_invalid_langs:
            print("{} is not a valid language abbreviation, "
                  "the languages' directory does not exist, "
                  "or it doesn't contain SVG files.".format(lang),
                  file=sys.stderr)

    if count_langs_done == 0:
        print("Nothing to render detected.", file=sys.stderr)
    else:
        print("Rendered episode in {} languages".format(count_langs_done))


if __name__ == "__main__":
    main()

