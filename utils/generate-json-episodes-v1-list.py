#!/usr/bin/env python
# compatible with python2 and python3, no dependencies except python itself

import sys
from glob import glob
from os import path
import json

# take as the first argument the episodes directory that we will inspect
webcomics_dir = sys.argv[1]

# helper function that takes a glob pattern as path parts, and returns an array of normalized paths
def globber(*pattern_path):
    return [
        path.normpath(p)
        for p
        in glob( path.join(*pattern_path) )
    ]

def split_page_name(pagepath):
  # page names are these (excluding parent directory/URI):
  #  comic pages (including title and credits): LANG_COMIC_by-ARTIST_E##P##.EXT
  #  cover pictures: LANG_COMIC_by-ARTIST_E##.EXT
  #  cover picture without text: COMIC_by-ARTIST_E##.EXT
  
  splat = path.basename(pagepath).split("_")
  if len(splat) == 3: # add phantom lang for cover picture without text
    splat.insert(0, "")
  filename = "_".join(splat[1:])
  
  lang = splat[0]
  comic = splat[1]
  artist = splat[2][3:]
  (epstr, ext) = splat[3].split('.')
  
  # + 1 to start after the letter
  ep_index = epstr.find('E') + 1
  page_index = epstr.find('P') + 1
  if page_index == 0:
    ep = epstr[ep_index:]
    page = ''
  else:
    ep = epstr[ep_index:page_index - 1] # stop at the letter, not after
    page = epstr[page_index:]
  
  return {'lang': lang, 'comic': comic, 'artist': artist, 'ep': ep, 'page': page, 'ext': ext, 'filename': filename}

def assert_dict_equal(a, b):
  equal = set(a.items()) ^ set(b.items())
  if len(equal) != 0:
    print("NOT EQUAL:")
    print(a)
    print(b)

# TODO: tests outside REPL
#def test_parser():
#  # TODO: pass bad input
#assert_dict_equal(split_page_name("/test/Pepper-and-Carrot_by-David-Revoy_E04.jpg"), {'lang': '', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '', 'ext': 'jpg'})
#assert_dict_equal(split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04.jpg"), {'lang': 'en', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '', 'ext': 'jpg'})
#  #split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04.jpg")
#assert_dict_equal(split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04P00.jpg"), {'lang': 'en', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '00', 'ext': 'jpg'})
#assert_dict_equal(split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04P01.jpg"), {'lang': 'en', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '01', 'ext': 'jpg'})
#assert_dict_equal(split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04P02.jpg"), {'lang': 'en', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '02', 'ext': 'jpg'})
#assert_dict_equal(split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04P03.jpg"), {'lang': 'en', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '03', 'ext': 'jpg'})
#assert_dict_equal(split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04P04.jpg"), {'lang': 'en', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '04', 'ext': 'jpg'})
#assert_dict_equal(split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04P05.gif"), {'lang': 'en', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '04', 'ext': 'gif'})
#assert_dict_equal(split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04P06.jpg"), {'lang': 'en', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '04', 'ext': 'jpg'})
#assert_dict_equal(split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04P07.jpg"), {'lang': 'en', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '07', 'ext': 'jpg'})
#assert_dict_equal(split_page_name("/test/en_Pepper-and-Carrot_by-David-Revoy_E04P08.jpg"), {'lang': 'en', 'comic': "Pepper-and-Carrot", 'artist': "David-Revoy", 'ep': '04', 'page': '08', 'ext': 'jpg'})
#
#test_parser()

def find_episode_pages(episode_dir):
  page_parts = [split_page_name(path) for path in globber(episode_dir, 'low-res', 'en*Pepper*.*')]
  pages = {}
  credits = len(page_parts) - 2 # remove cover and title
  
  for p in page_parts:
    id = p['page']
    if id == "":
      id = "cover"
    elif int(id) == 0:
      id = 'title'
    elif int(id) == credits:
      id = 'credits'
    else:
      id = int(id)
    pages[id] = p['filename']
  
  return pages

#print(find_episode_pages("ep04_Stroke-of-genius"))

# generate our data structure by walking through the webcomics directory
episodes_data = [
    {
        'name':                 path.basename(episode_dir),
        'pages':                find_episode_pages(episode_dir),
        'total_pages':          len(find_episode_pages(episode_dir)) - 2, # don't count the last page (credits)
        'translated_languages': [
            path.basename(lang_dir)
            for lang_dir
            in sorted( globber(episode_dir, 'lang', '*/') )
        ],
    }
    for episode_dir
    in sorted( globber(webcomics_dir, 'ep*_*/') )
]

# write the data structure to stdout in JSON format
sys.stdout.write( json.dumps(episodes_data, indent=4) )
