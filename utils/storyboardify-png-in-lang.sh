#!/bin/bash

if [ -d "$1" ]; then
  cd "$1"
  for pngfile in *.png; do
    echo "$pngfile (before):"
    du -k "$pngfile" | cut -f 1
      convert "$pngfile" -resize 694x981 -colors 64 "$pngfile"
      optipng -o7 "$pngfile"
    echo "$pngfile (after):"
      du -k "$pngfile" | cut -f 1
    echo ""
    echo "------------------"
    echo ""
  done
elif [ -f "$1" ]; then
    pngfile="$1"
    echo "$pngfile (before):"
    du -k "$pngfile" | cut -f 1
      convert "$pngfile" -resize 694x981 -colors 64 "$pngfile"
      optipng -o7 "$pngfile"
    echo "$pngfile (after):"
      du -k "$pngfile" | cut -f 1
    echo ""
    echo "------------------"
    echo ""
else
  echo "Folder not found."
  echo "Script usage: ./storyboardify-png-in-lang.sh /absolute/path/to/the/folder/containing/png/to/optimize"
  echo "Or"
  echo "Script usage: ./storyboardify-png-in-lang.sh /absolute/path/to/a/png/file/to/optimize.png"
  echo ""
  echo ""
  exit
fi


