#!/usr/bin/env python
# -*- coding: utf-8 -*-
# compatible with python2 and python3, no dependencies except python itself

import sys
from glob import glob
from os import path
import json

def globber(*pattern_path):
    # return a list of all the normalized paths expanded with glob from the reassembled path pattern
    return [
        path.normpath(p)
        for p
        in glob( path.join(*pattern_path) )
    ]

def episode_pagecount(episode_dir):
    # returns the number of files that correspond to pages
    return len(
        globber(episode_dir, 'lang', 'gfx_*_E*P*.png')
    ) - 1
    # minus one because we don't count the last page (credits)

def episode_languages(episode_dir):
    # returns the names of all directories in the 'lang' directory of the episode
    return [
        path.basename(lang_dir)
        for lang_dir
        in sorted( globber(episode_dir, 'lang', '*/') )
    ]

def episodes_metadata(webcomics_dir):
    # return metadata for each episode in the directory we get as a parameter
    return [
        {
            'name':                 path.basename(episode_dir),
            'total_pages':          episode_pagecount(episode_dir),
            'translated_languages': episode_languages(episode_dir)
        }
        for episode_dir
        in sorted( globber(webcomics_dir, 'ep*_*/') )
    ]

# Dump, in JSON format…
json.dump(
    # …the episodes' metadata generated from the episodes directory that we take as the first argument of the script…
    episodes_metadata(sys.argv[1]),
    sys.stdout, # …to the standard output…
    indent=4    # …indenting the JSON with four spaces.
)
