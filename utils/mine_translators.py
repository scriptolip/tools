#!/usr/bin/env python3
# encoding: utf-8
#
#  mine_translators.py
#
#  SPDX-License-Identifier: GPL-3.0-or-later
#
#  Copyright 2020 GunChleoc <fios@foramnagaidhlig.net>
#
# Needs https://github.com/gitpython-developers/GitPython

"""Mine translator info from git log.

Needs to be called from the webcomics repo base directory.
"""

import cgitb
import codecs
from collections import defaultdict
import json
import os.path
import sys

import git

# For nice debug logs
cgitb.enable(format='text')


def load_json(filename):
    """If filename exists, load json from it."""
    result = dict()
    if os.path.isfile(filename):
        try:
            jsonfile = codecs.open(filename, encoding='utf-8', mode='r')
            result = json.load(jsonfile)
        except Exception as error:
            print('Error reading json from file %s:' % filename)
            print('\t', error)
        jsonfile.close()
    return result


def main():
    """Collects authors for SVG files from git log and writes them with their
    commit subjects to info.json files, one for each directory found."""

    print('#################################################################')
    print('#  Tool for collecting git authors for lang/<locale>/info.json  #')
    print('#################################################################')

    # The first argument is the script name itself, so we expect 2 of them
    if len(sys.argv) != 2:
        print('Wrong number of arguments! Usage:')
        print('    mine_translators.py <path>')
        print('For example:')
        print('    mine_translators.py ep01_Potion-of-Flight')
        sys.exit(1)

    cwd = os.getcwd()
    base_path = os.path.abspath(os.path.join(cwd, sys.argv[1]))

    # Reference template for new files
    template_filename = os.path.abspath(
        os.path.join(cwd, '.ci', 'template_lang_info.json'))
    if not os.path.isfile(template_filename):
        print('Unable to find %s in path %s' %
              ('.ci/template_lang_info.json', cwd))
        print('You can call it from the webcomics base path like this: '
              '../tools/utils/mine_translators.py <dir-to-check>')
        sys.exit(1)

    json_template = load_json(template_filename)
    # Delete notes key from template
    json_template.pop('notes', None)

    g = git.Git(base_path)

    print('Walking %s' % base_path)

    for (dirpath, _, filenames) in os.walk(base_path):
        authors = defaultdict(list)
        for filename in filenames:
            if filename.endswith('.svg'):
                gitlog = g.log('--follow', '--format="%an----------%s"',
                               os.path.join(dirpath, filename))
                for entry in gitlog.splitlines():
                    author_and_message = entry.strip('"').split('----------')
                    if len(author_and_message) == 2:
                        author = author_and_message[0]
                        message = author_and_message[1]
                        if not message in authors[author]:
                            authors[author].append(message)

        if authors:
            # Get info.json if available, otherwise fall back to template
            info_path = os.path.join(dirpath, 'info.json')
            info_json = load_json(info_path)
            if not info_json:
                info_json = json_template

            info_json['TODO'] = dict()

            # Read existing authors
            existing_names = list()
            for category in info_json['credits']:
                for name in info_json['credits'][category]:
                    if not name in existing_names:
                        existing_names.append(name)

            # Add unknown authors
            for author in authors.keys():
                if author in existing_names:
                    continue
                info_json['TODO'][author] = authors[author]

            # Write file if we have TODO entries
            if info_json['TODO']:
                with codecs.open(info_path, encoding='utf-8', mode='w') as dest_file:
                    json.dump(info_json, dest_file, indent=3)
                    print('Wrote JSON to %s' % info_path)

    print('Done.')


# Call main function when this script is being run
if __name__ == '__main__':
    sys.exit(main())
