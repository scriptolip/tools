# Pepper & Carrot

![alt tag](lib/img/repository-header.jpg)

[![GPL](lib/img/license-badge.png)](LICENSE)

## About

This is the repository "tools" of the open-source webcomic [Pepper&Carrot](http://www.peppercarrot.com) project. This repo has a collection of scripts interacting with:

- The files of Pepper&Carrot (mostly 'webcomics' repo)

- Installed software (like Imagemagick, Unzip, Inkscape...)

- The Framagit repositories 

- The server of https://www.peppercarrot.com.

## Mission

The purpose of this scripts are to automate common tasks in order for me, David Revoy, to get more time drawing or painting the webcomic (or for translators to have an easier time translating the webcomics). The scripts might also help at exposing or digesting the data of the project's server or repositories to external community projects.

## Specifications for contributors

- Bash language is prefered.

- JSON is prefered for databases (using [jo](https://github.com/jpmens/jo)).

- One script does one task and do it well (a global script can call later other small scripts).

- User interface can be added via simple Zenity questions or asking input in CLI directly.

- Verbose output with useful infos is appreciated.

- Colored and good looking output make it easier to read (esp. yellow for titles/ green for validation/ red for errors).

- Scripts must be commented generously in English to know what line do what.

- Scripts might use tabs or space(as you want, no opinions about it as long as it is readable).

- Scripts shouldn't use nested functions that loop over themselves inside a third function named with abstract small names.

- Scripts shouldn't be complexified on the only purpose to do economy of lines in the script, or to compress three lines of math into one for the beauty of it.

- Prefer human readibility over computer performance or code/sandard correctness.

### But why this KISS baby-code constrain?

In the past and another project long time ago, a contributor rewrote my repository -a project wrote in Bash- into a C program for better security and performance. Another one rewrote a second of my Bash repo into Tk for a better user interface. In both case, I merged the refactor into the official project. I was thinking the rewrites were superiors and great for the project. They did in a objective way and in short term. In the long term, both projects died a couple years after the rewrite when I wasn't able to maintain the work of the rewriters. The two projects were dead for the same reason, and you can think of it as a good method if you want to extinguish an open-source project (joke). Looking back at it now, I should have been asking to both rewriter to help my repo with the way it was already written, with my Bash limitations, with my taste and specifications. For a rewrite; asking them to fork the project on their external repo would have been probably the best option. That's why, after this experience and other conflict I met linked to the same type of conflict, I decide to add this rules to Pepper&Carrot tools. Because I'll -sooner or later- will be alone at extending, fixing or just maintaining this folder of scripts. Better to anticipate this future now and be easy with the future myself who , I'm sure, will invest his time more into learning new proportions, anatomy, brushes things than learning another programming language.

All in all, this text is not something against receiving external development contribution and help, corrections, improvements but it needs to be simply adapted to my level skill. That's why I call it the "KISS baby-code constrain" because I know how humble my skill is. That can feels awkward for advanced developper out there but in my opinion "Who can do the most, can also do the less" so it shouldn't be a problem to do things this way.

### Structure of scripts

Here is a paragraph if you have trouble to think like inside the brain of a beginner scripter. I'm adding this one for fun, but it will probably help at clarifying my skill. Think about robots arms working all along a long conveyor belt. The belt has a start, a middle, an end. Data jumps on the belt at the start, and are transformed by robots meters after meters on the belt. If you follow such a belt in real life, you'll understand what's going-on even without a lot of experience in engineering. That metaphor illustrate my taste for the structure of my KISS baby-coding scripts: the belt flows often from the top to the bottom of the script with sometime exeptions (loops during the 'while', redirection with 'if', etc...). Simple functions that transform an input to an output are like the robots on the belt. It's a very modular, beginner friendly and easy to understand on my side.

## Install

To perform a full install of the project on a GNU/Linux system, you'll need:

**Dependencies/Libraries**

Only free/libre and open-sources tools :

* **Bash** ( >= 4.3.11 ) _Command line language in terminal._
* **Python** ( >= 2.6 ) _Programming language that has convenient libraries for everything._
* **Git** ( >= 1.9.1 ) _Utility to manage distributed revision control_
* **Inkscape** ( >= 1.0 ) _Vector graphics software, used for SVG translation source files._
* **Imagemagick** ( >= 6.7.7.10 ) _Utility to manipulate images._
* **Zenity** ( >= 3.8.0 ) _Utility to create simple graphical user interface dialogs using GTK._
* **Unzip** ( >= 6.0 ) _Utility to extract ZIP files._
* **Wget** ( >= 1.15 ) _Utility to download files._
* **Diff** ( >= 3.3 ) _Utility to compare files and directories._
* **Parallel** ( >= 20130922 ) _Utility to exectute jobs in parallel._
* **Notify-send** ( >= 0.7.6 ) _Utility to send desktop notifications. in notify-osd and libnotify-bin_
* **Lftp** ( >= 4.6.3 ) _Utility to perform FTP transfers._
* **Optipng** ( >= 0.7.6 ) _Utility to compress PNGs images._
* **Rsync** ( >= 3.1.2 ) _Utility to transfert, copy, sync files and folders._
* **Sshpass** ( >= 1.06 ) _Utility allowing convenient usage of SSH password when used with Rsync_ 

Eg. on Debian/'buntu 18.04:

```
sudo apt install gimp git wget unzip imagemagick inkscape zenity parallel diffutils rsync lftp notify-osd libnotify-bin optipng sshpass
```

If the script doesn't launch; check the log at peppercarrot/tools/log, this file contain hint about the missing dependency that prevent the script of running.

**Repositories**

For installing one of the modules of Pepper&Carrot, you can do something like this:
```
mkdir peppercarrot
cd peppercarrot
git clone https://framagit.org/peppercarrot/webcomics.git
git clone https://framagit.org/peppercarrot/tools.git
git clone https://framagit.org/peppercarrot/website.git
git clone https://framagit.org/peppercarrot/scenarios.git
git clone https://framagit.org/peppercarrot/book.git
git clone https://framagit.org/peppercarrot/wiki.git
git clone https://framagit.org/peppercarrot/fonts.git

```

The scripts in the **tools** repository/folder will help you to maintain all the modules.

Before start, copy the **config.sample** file in to **config**
```
cd tools
cp config.sample config

```
Then edit **config** to match your preferences and path.

**Font instalation**

The script 'update-all' (you'll find a shortcut to it on the launcher) will take care to sync the repo to the latest. But I advice on GNU/Linux to create a link between the fonts folder and the system fonts with a line like that:

```
ln -s ~/peppercarrot/fonts/ ~/.fonts/peppercarrot-fonts
```

## License

#### Scripts
License [GPLv3 or later](http://gnu.org/licenses/gpl.html). This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version. This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this program (LICENSE file at the root).  If not, see [http://www.gnu.org/licenses/](http://www.gnu.org/licenses/).

#### Artworks
Artworks, Pepper&Carrot logo and title artwork in this repository are licensed under the [Creative Commons Attribution 4.0](https://creativecommons.org/licenses/by/4.0/)
 to David Revoy, [www.peppercarrot.com](http://www.peppercarrot.com).
