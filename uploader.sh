#!/bin/bash
# Information

export script_title="Uploader"
export script_version="2.0a"
export script_filename="uploader.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config
function_dependency_check sshpass rsync

# Script
function_decorative_header

# Rsync task, by priorities to compensate slow connections
# (note: add --dry-run to test new things here)
echo ""
echo "${Blue}[LOW-RES Comic]${Off}"
echo "${Blue}=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
rsync -avzh --progress --delete \
       --exclude '.git' \
       --exclude '.gitignore' \
       --exclude 'last_updated.txt' \
       --exclude '.directory' \
       --exclude '0_test' \
       --exclude '0_archives' \
       --exclude 'fonts' \
       --exclude 'animation' \
       --exclude 'backup' \
       --exclude 'cache' \
       --exclude 'wip' \
       --exclude '*.kra' \
       --exclude '*.ora' \
       --exclude '*.blend' \
       --exclude '*.sh' \
       --exclude '*~' \
       --exclude '-=-=-=-=-=-=-=-=-=-=-=-=-=-' \
       --exclude '*.json' \
       --exclude '0ther' \
       --exclude 'hi-res' \
       --exclude 'lang' \
       --exclude 'zip' \
       --exclude 'single-page' \
       --exclude 'gfx-only' \
       --exclude 'txt-only' \
       --rsh="sshpass -p $server_password ssh -o StrictHostKeyChecking=no"  \
       "$project_root"/"$folder_webcomics"/ "$server_username@$server_host_adress":"www/0_sources" \
       | grep -E --color=never '^deleting|[^/]$'
echo "${Off}"

echo ""
echo "${Green}[HI-RES Comic]${Off}"
echo "${Green}=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
rsync -avzh --progress --delete \
       --exclude '.git' \
       --exclude '.gitignore' \
       --exclude 'last_updated.txt' \
       --exclude '.directory' \
       --exclude '0_test' \
       --exclude '0_archives' \
       --exclude 'fonts' \
       --exclude 'animation' \
       --exclude 'backup' \
       --exclude 'cache' \
       --exclude 'wip' \
       --exclude '*.kra' \
       --exclude '*.ora' \
       --exclude '*.blend' \
       --exclude '*.sh' \
       --exclude '*~' \
       --exclude '-=-=-=-=-=-=-=-=-=-=-=-=-=-' \
       --exclude '*.json' \
       --exclude '0ther' \
       --exclude 'low-res' \
       --exclude 'lang' \
       --exclude 'zip' \
       --exclude 'single-page' \
       --exclude 'gfx-only' \
       --exclude 'txt-only' \
       --rsh="sshpass -p $server_password ssh -o StrictHostKeyChecking=no"  \
       "$project_root"/"$folder_webcomics"/ "$server_username@$server_host_adress":"www/0_sources" \
       | grep -E --color=never '^deleting|[^/]$'
echo "${Off}"

echo ""
echo "${Purple}[SOURCES:GFX-ONLY,SINGLEPAGE]${Off}"
echo "${Purple}=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
rsync -avzhe ssh --progress --delete \
       --exclude '.git' \
       --exclude '.gitignore' \
       --exclude 'last_updated.txt' \
       --exclude '.directory' \
       --exclude '0_test' \
       --exclude '0_archives' \
       --exclude 'fonts' \
       --exclude 'animation' \
       --exclude 'backup' \
       --exclude 'cache' \
       --exclude 'wip' \
       --exclude '*.kra' \
       --exclude '*.ora' \
       --exclude '*.blend' \
       --exclude '*.sh' \
       --exclude '*~' \
       --exclude '-=-=-=-=-=-=-=-=-=-=-=-=-=-' \
       --exclude '*.json' \
       --exclude '0ther' \
       --exclude 'lang' \
       --exclude 'zip' \
       --exclude 'txt-only' \
       --rsh="sshpass -p $server_password ssh -o StrictHostKeyChecking=no"  \
       "$project_root"/"$folder_webcomics"/ "$server_username@$server_host_adress":"www/0_sources" \
       | grep -E --color=never '^deleting|[^/]$'
echo "${Off}"

echo ""
echo "${Yellow}[SOURCES END:OTHERS,JSON,ZIP,TXT-ONLY and SVGs]${Off}"
echo "${Yellow}=-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-="
rsync -avzh --progress --delete \
       --exclude '.git' \
       --exclude '.gitignore' \
       --exclude 'last_updated.txt' \
       --exclude '.directory' \
       --exclude '0_test' \
       --exclude '0_archives' \
       --exclude 'fonts' \
       --exclude 'animation' \
       --exclude 'backup' \
       --exclude 'cache' \
       --exclude 'wip' \
       --exclude '*.kra' \
       --exclude '*.ora' \
       --exclude '*.blend' \
       --exclude '*.sh' \
       --exclude '*~' \
       --exclude '-=-=-=-=-=-=-=-=-=-=-=-=-=-' \
       --rsh="sshpass -p $server_password ssh -o StrictHostKeyChecking=no"  \
       "$project_root"/"$folder_webcomics"/ "$server_username@$server_host_adress":"www/0_sources" \
       | grep -E --color=never '^deleting|[^/]$'
echo "${Off}"

echo ""
echo "* Regenerate last_updated.txt"
date +'%s' > "$project_root"/"$folder_webcomics"/last_updated.txt
date +'%d/%m/%Y%t%H:%M:%S' >> "$project_root"/"$folder_webcomics"/last_updated.txt
cat "$project_root"/"$folder_webcomics"/last_updated.txt
rsync -avzh --progress \
       --rsh="sshpass -p $server_password ssh -o StrictHostKeyChecking=no"  \
       "$project_root"/"$folder_webcomics"/last_updated.txt "$server_username@$server_host_adress":"www/0_sources" \
       | grep -E --color=never '^deleting|[^/]$'

echo "File transfert end." | tee -a "$log_file"

# Footer library
function_load_source_lib "$script_absolute_path"/lib/footer.sh

if [ "$1" = "--prompt" ]; then
# Task is executed inside a terminal window (pop-up)
# This line prevent terminal windows to be auto-closed
# and necessary to read log later, or to reload the script
 echo "${Purple}Press [Enter] to exit or [r](then Enter) to reload $script_filename ${Off}"
 read -p "?" ANSWER
 if [ "$ANSWER" = "r" ]; then
   "$project_root"/"$folder_tools"/"$script_filename" --prompt
 fi
fi
