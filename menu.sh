#!/bin/bash

# Information
export script_title="Pepper&Carrot main menu"
export script_version="2.0a"
export script_filename="menu.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config
function_dependency_check convert zenity python lftp x-terminal-emulator "$text_editor" "$file_browser"

cd "$project_root"/"$folder_webcomics"

# Function to populate and display the main menu listing with list of episodes and main options.
function_main_menu () {
  
  items+=( "‣ Render all" )
  items+=( "‣ Uploader" )
  items+=( " " )
  for directories in $(find $project_root/$folder_webcomics -maxdepth 1 -type d -printf '%f\n' | sort -r ); do
    episodefolder=$directories
    if [[ $episodefolder == "new-"* ]]; then
      items+=( "$episodefolder"* )
    fi
  done
  if [ -d "$project_root"/"$folder_webcomics"/New ]; then
    items+=( "New"* )
  fi
  for directories in $(find $project_root/$folder_webcomics -maxdepth 1 -type d -printf '%f\n' | sort -r ); do
    episodefolder=$directories
    if [[ $episodefolder == "ep"* ]]; then
      items+=( "$episodefolder"* )
    fi
  done
  items+=( "+ Add a new episode" )
  items+=( " " )
  items+=( "‣ Website" )
  items+=( "‣ Wiki & Doc" )
  items+=( "‣ Community update" )
  items+=( "‣ Validate transcript" )
  items+=( "‣ episodes.json" )

  menuchoice=$(zenity --list --title="$script_title" \
              --width=400 --height=600 --window-icon="$folder_tools/lib/new-episode_template/img/peppercarrot_icon.png" \
              --text='Select an episode or an action' \
              --column='menu' "${items[@]}");

  # Method to avoid a Zenity bug with trailing | on option selected
  clear
  menuchoicecleaned=${menuchoice%|*}
}

# Function to populate and display the sub menu when a episode is selected with episode options
function_launch_render() 
{
  echo "=> rendering script"
  echo "$project_root"/"$folder_webcomics"/"$menuchoicecleaned"/
  function_run_in_terminal "$project_root/$folder_tools/renderfarm.sh $project_root/$folder_webcomics/$menuchoicecleaned/" &
}

# Script
function_main_menu

# Analyse the output string of Zenity main menu and adapt behavior for each case.
if [ "$menuchoicecleaned" = " " ]; then
  echo "${Green}* mistake!!  ${Off}"
  zenity --error --text "You selected a spacer, try again."; echo $?
    
elif [ "$menuchoicecleaned" = "" ]; then
  # OK or Cancel is pressed without selection, we exit.
  exit

elif [ "$menuchoicecleaned" = "‣ episodes.json" ]; then
  # Generate episodes-list.md and episodes.json
  # Remove previous markdown generated before regenerating
  cd "$project_root"/"$folder_webcomics"
  rm "$project_root"/"$folder_webcomics"/.episodes-list.md
  rm "$project_root"/"$folder_webcomics"/episodes.json
  # Generate .episode-list.md
  for directories in $(find $project_root/$folder_webcomics -maxdepth 1 -type d -printf '%f\n' | sort ); do
    episodefolder=$directories
    if [[ $episodefolder == *"ep"* ]]; then
      cd "$project_root"/"$folder_webcomics"
      echo "$episodefolder" >> "$project_root"/"$folder_webcomics"/.episodes-list.md
    fi
  done

  # Color any episodes*.json generation errors in red
  echo "$Red"

  # Generate episodes.json
  if "$project_root/$folder_tools/utils/generate-json-episodes-list.py" \
     "$project_root/$folder_webcomics" \
     > "$project_root/$folder_webcomics/episodes.json.tmp"
  then
    mv "$project_root/$folder_webcomics/episodes.json.tmp" \
       "$project_root/$folder_webcomics/episodes.json"
  fi
  # Generate episodes-v1.json
  if "$project_root/$folder_tools/utils/generate-json-episodes-v1-list.py" \
     "$project_root/$folder_webcomics" \
     > "$project_root/$folder_webcomics/episodes-v1.json.tmp"
  then
    mv "$project_root/$folder_webcomics/episodes-v1.json.tmp" \
       "$project_root/$folder_webcomics/episodes-v1.json"
  fi
  
  # Generate temporary metadata.json for test only
  cd "$project_root"/"$folder_webcomics"
  UNICODE_JSON= "$project_root"/"$folder_webcomics"/.ci/metadata.py "$project_root"/"$folder_webcomics" > "$project_root"/"$folder_webcomics"/metadata.json

  # Back to no color
  echo "$Off"

  break
  exit
  
elif [ "$menuchoicecleaned" = "‣ Uploader" ]; then
  function_run_in_terminal "$project_root/$folder_tools/uploader.sh"
 
elif [ "$menuchoicecleaned" = "‣ Website" ]; then
  function_run_in_terminal "$project_root/$folder_tools/update-website.sh"
  
elif [ "$menuchoicecleaned" = "‣ Wiki & Doc" ]; then
  function_run_in_terminal "$project_root/$folder_tools/update-wiki-and-doc.sh"

elif [ "$menuchoicecleaned" = "‣ Community update" ]; then
  function_run_in_terminal "$project_root/$folder_tools/update-community.sh"

elif [ "$menuchoicecleaned" = "‣ Validate transcript" ]; then
  function_run_in_terminal "$project_root/$folder_tools/validate-transcript-wrapper.sh"
  
elif [ "$menuchoicecleaned" = "‣ Render all" ]; then
  function_run_in_terminal "$project_root/$folder_tools/update-all.sh --prompt"
      
elif [ "$menuchoicecleaned" = "+ Add a new episode" ]; then
  function_run_in_terminal "$project_root/$folder_tools/add-a-new-episode.sh"

else 
  # Cheap method to open the sub menu for episodes options 
  function_launch_render
fi

# Footer library
function_load_source_lib "$script_absolute_path"/lib/footer.sh
