#!/bin/bash

#: Title       : Pepper&Carrot Community git project updater
#: Author      : David REVOY < info@davidrevoy.com >
#: License     : GPL

# Information
export script_title="Community updater"
export script_version="0.1a"
export script_filename="update-community.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
    else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config
function_dependency_check zip git wget lftp inkscape

# Script
function_decorative_header

function_decorative_title "Peppercarrot mini"
# ===========================================

export pcmini_gitpath="derivations/webcomic/peppercarrot_mini"
export pcmini_webpath="webcomics/0ther/community/Pepper-and-Carrot-Mini_by_Nartance"
export folder_render="render"
export render_jpg_size="910x"
export project_attribution="by-Nartance"


cd "$project_root"/"$pcmini_gitpath"/
function_git_update "$project_root"/"$pcmini_gitpath"/

# Create the render folder, if not existing.
if [ -d "$project_root"/"$pcmini_gitpath"/"$folder_render" ]; then
    echo " * $folder_render found" 
else
    echo "${Green} * creating folder: $folder_render ${Off}"
    mkdir -p "$workingpath"/"$folder_render"
fi

# P&C Mini renderfarm:
# Update JSON
if [ -f "$project_root"/"$pcmini_webpath"/links.json ]; then
  rm -f "$project_root"/"$pcmini_webpath"/links.json
fi
cp "$project_root"/"$pcmini_gitpath"/links.json "$project_root"/"$pcmini_webpath"/links.json

cd "$project_root"/"$pcmini_gitpath"/lang/
# Scan all lang
for isolang in "$project_root"/"$pcmini_gitpath"/lang/*/; do
    if [[ -d "$isolang" && ! -L "$isolang" ]]; then
        isolang=${isolang%*/}
        isolang=${isolang##*/}
        cd "$project_root"/"$pcmini_gitpath"/lang/"$isolang"/
        echo "${Blue} * Processing Pepper&Carrot mini $isolang folder: ${Off}"
        # Scan all svg
        for svgfile in *.svg; do
            if [ -f "$project_root"/"$pcmini_gitpath"/lang/"$isolang"/"$svgfile" ]; then
                echo "${Green}Found valid [$isolang] svg: $svgfile${Off}"
                pngfile=$(echo $svgfile|sed 's/\(.*\)\..\+/\1/')".png"
                jpgfile=$(echo $svgfile|sed 's/\(.*\)\..\+/\1/')"P01_$project_attribution.jpg"
                # Rendering
                inkscape "$project_root"/"$pcmini_gitpath"/lang/"$isolang"/"$svgfile" -o "$project_root"/"$pcmini_gitpath"/"$folder_render"/"$isolang"_"$pngfile"
                convert -strip -interlace Plane -colorspace sRGB -units PixelsPerInch "$project_root"/"$pcmini_gitpath"/"$folder_render"/"$isolang"_"$pngfile" -density 300 -colorspace sRGB -background white -alpha remove -define png:compression-level=1 "$project_root"/"$pcmini_gitpath"/"$folder_render"/"$isolang"_"$pngfile"
                convert "$project_root"/"$pcmini_gitpath"/"$folder_render"/"$isolang"_"$pngfile" -resize "$render_jpg_size" -unsharp 0x0.50+0.75+0 -colorspace sRGB -quality 92% "$project_root"/"$pcmini_gitpath"/"$folder_render"/"$isolang"_"$jpgfile"
                cp "$project_root"/"$pcmini_gitpath"/"$folder_render"/"$isolang"_"$jpgfile" "$project_root"/"$pcmini_webpath"/"$isolang"_"$jpgfile"
            fi
        done
    fi
done

# Update About markdown project's descriptor:
cd "$project_root"/"$pcmini_gitpath"/about/
for mdfilepath in `find . -name "*.md"`; do
    if [ -f "$project_root"/"$pcmini_gitpath"/about/"$mdfilepath" ]; then
        cp "$mdfilepath" "$project_root"/"$pcmini_webpath"/"$mdfilepath"
    fi
done

# Update Cover artwork:
cd "$project_root"/"$pcmini_gitpath"/img/_cover/
convert pepper_mini_vs_real_pepper.png -resize 1280x720 -unsharp 0x0.50+0.75+0 -colorspace sRGB -quality 88%  "$project_root"/"$pcmini_webpath"/00_cover.jpg
echo "* done"

# Footer library
function_load_source_lib "$script_absolute_path"/lib/footer.sh

if [ "$1" = "--prompt" ]; then
    # Task is executed inside a terminal window (pop-up)
    # This line prevent terminal windows to be auto-closed
    # and necessary to read log later, or to reload the script
    echo "${Purple}  Press [Enter] to exit or ${White}${BlueBG}[r]${Off} to reload or ${White}${BlueBG}[u]${Off} to upload all the change. ${Off}"
    read -p "?" ANSWER
    if [ "$ANSWER" = "r" ]; then
        "$project_root"/"$folder_tools"/"$script_filename" --prompt
    elif [ "$ANSWER" = "u" ]; then
        "$project_root"/"$folder_tools"/uploader.sh --prompt
    fi
fi
