#!/bin/bash

# Information
export script_title="Website update"
export script_version="0.1a"
export script_filename="update-website.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config
function_dependency_check git rsync lftp sshpass

# Script
function_decorative_header

# website
echo "Routine update website repositories"
function_git_update "$project_root"/"$folder_website"
# Compile all PO into MO, each time to be sure.
bash "$project_root"/"$folder_website"/po/compile_all_PO_into_MO.sh
bash "$project_root"/"$folder_websitetest"/po/extract_translation_percent_completion.sh

if [ -d "$project_root"/"$folder_websitetest" ]; then

  function_decorative_title "TEST.PEPPERCARROT.COM"
  rm "$project_root"/"$folder_websitetest"/robots.txt
  function_git_update "$project_root"/"$folder_websitetest"

  echo ""
  echo "* Post-install fix"
    # sync template database
    # check directory structure
    if [ ! -d "$project_root"/"$folder_websitetest"/data ]; then
      mkdir -p "$project_root"/"$folder_websitetest"/data
    fi
    if [ ! -d "$project_root"/"$folder_websitetest"/0_sources ]; then
      mkdir -p "$project_root"/"$folder_websitetest"/0_sources
      mkdir -p "$project_root"/"$folder_websitetest"/0_sources/0ther/misc
      mkdir -p "$project_root"/"$folder_websitetest"/0_sources/0ther/sketchbook
      mkdir -p "$project_root"/"$folder_websitetest"/0_sources/0ther/book-publishing
      echo "* Sync things"
      rsync -arltDvu --progress --delete --ignore-errors "$project_root"/"$folder_websitetest"/.data/ "$project_root"/"$folder_websitetest"/data
      rsync -arltDvu --progress --delete --ignore-errors "$project_root"/"$folder_websitetest"/.0_sources/ "$project_root"/"$folder_websitetest"/0_sources
      rsync -arltDvu --progress --delete --ignore-errors "$project_root"/"$folder_websitetest"/.0_sources/0ther/artworks/ "$project_root"/"$folder_websitetest"/0_sources/0ther/misc
      rsync -arltDvu --progress --delete --ignore-errors "$project_root"/"$folder_websitetest"/.0_sources/0ther/artworks/ "$project_root"/"$folder_websitetest"/0_sources/0ther/sketchbook
      rsync -arltDvu --progress --delete --ignore-errors "$project_root"/"$folder_websitetest"/.0_sources/0ther/artworks/ "$project_root"/"$folder_websitetest"/0_sources/0ther/book-publishing
    fi
    # sync template into prod for the test
    cp "$project_root"/"$folder_websitetest"/.gitignore.template "$project_root"/"$folder_websitetest"/.gitignore

    # copy the configuration
    cp "$project_root"/"$folder_websitetest"/configure.php.new "$project_root"/"$folder_websitetest"/configure.php

    # Compile all PO into MO, each time to be sure.
    bash "$project_root"/"$folder_websitetest"/po/compile_all_PO_into_MO.sh
    bash "$project_root"/"$folder_websitetest"/po/extract_translation_percent_completion.sh

    # block robots on test.peppercarrot.com: they creates too much traffic
rm "$project_root"/"$folder_websitetest"/robots.txt
cat > "$project_root"/"$folder_websitetest"/robots.txt<< EOF
User-agent: *
Disallow: /
EOF

    # update last_updated.txt for the cache
    date +'%s' > "$project_root"/"$folder_websitetest"/0_sources/last_updated.txt
    date +'%d/%m/%Y%t%H:%M:%S' >> "$project_root"/"$folder_websitetest"/0_sources/last_updated.txt

  echo "UPLOAD TO TEST.PEPPERCARROT WEBSITE"
  function_decorative_split
  # (note: add --dry-run to test new things here)
  rsync -avzhe ssh --progress --delete \
       --exclude '.git' \
       --exclude '.gitignore' \
       --exclude '.gitignore.template' \
       --exclude 'configure.php.new' \
       --exclude '.htaccess.template' \
       --exclude '.directory' \
       --exclude '/.0_sources' \
       --exclude '/cache' \
       --exclude '/.data' \
       --rsh="sshpass -p $server_password ssh -o StrictHostKeyChecking=no"  \
       "$project_root"/"$folder_websitetest"/ "$server_username@$server_host_adress":"test" \
       | grep -E --color=never '^deleting|[^/]$'
    cat "$project_root"/"$folder_websitetest"/0_sources/last_updated.txt
fi

echo ""
echo "${Purple}  UPLOAD CHANGES INTO PRODUCTION? ${Off}"
echo "${Purple}  [y] to upload the changes to official www.peppercarrot.com, [n] or any other key to exit ${Off}"
read -p "?" ANSWER
 
if [ "$ANSWER" = "y" ]; then
  function_decorative_title "UPLOAD TO OFFICIAL WWW.PEPPERCARROT.COM"
  # (note: add --dry-run to test new things here)
  rsync -avzhe ssh --progress --delete \
         --exclude '.git' \
         --exclude '.gitignore' \
         --exclude '.gitignore.template' \
         --exclude 'configure.php.new' \
         --exclude '.directory' \
         --exclude '/0_sources' \
         --exclude '/.0_sources' \
         --exclude '/data' \
         --exclude '/.data' \
         --exclude '/cache' \
         --exclude '/ext' \
         --exclude '/extras' \
         --exclude '/picam' \
         --exclude '/test' \
         --rsh="sshpass -p $server_password ssh -o StrictHostKeyChecking=no"  \
         "$project_root"/"$folder_website"/ "$server_username@$server_host_adress":"www" \
         | grep -E --color=never '^deleting|[^/]$'
  
  echo ""
  echo "* Regenerate cache (last_updated.txt)"
    date +'%s' > "$project_root"/"$folder_webcomics"/last_updated.txt
    date +'%d/%m/%Y%t%H:%M:%S' >> "$project_root"/"$folder_webcomics"/last_updated.txt
    cat "$project_root"/"$folder_webcomics"/last_updated.txt
    rsync -avzhe ssh --progress \
       --rsh="sshpass -p $server_password ssh -o StrictHostKeyChecking=no"  \
       "$project_root"/"$folder_webcomics"/last_updated.txt "$server_username@$server_host_adress":"www/0_sources" \
       | grep -E --color=never '^deleting|[^/]$'
fi

# Footer library
function_load_source_lib "$script_absolute_path"/lib/footer.sh

if [ "$1" = "--prompt" ]; then
# Task is executed inside a terminal window (pop-up)
# This line prevent terminal windows to be auto-closed
# and necessary to read log later, or to reload the script
 echo "${Purple}  Press [Enter] to exit or [r](then Enter) to reload $script_filename${Off}"
 read -p "?" ANSWER
 if [ "$ANSWER" = "r" ]; then
   "$project_root"/"$folder_tools"/"$script_filename" --prompt
 fi
fi
