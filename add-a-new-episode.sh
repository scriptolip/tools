#!/bin/bash

#: Title       : Add a new episode
#: Author      : David REVOY < info@davidrevoy.com >
#: License     : GPL

# Information
export script_title="Add a new episode"
export script_version="2.0a"
export script_filename="add-a-new-episode.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config

# Script

# Display a menu with basic questions:
function_menu () {
    cd "$project_root"/"$folder_webcomics"
    echo -n "${Green} Enter episode number (XX, 01 to 99) to generate ${Off}"
    read episodenumber
    echo "${Green} Pepper&Carrot episode $episodenumber ${Off}"
    echo -n "${Green} ... with how many page (1~9) ? ${Off}"
    read pagemax
    echo "${Green} $pagemax pages, OK. Working : ${Off}"
}


function_create_new_episode () {
    cd "$project_root"/"$folder_webcomics"
    if [ -d "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here ]; then
        echo "${Red}==> [error] New directory already exist. Rename it and restart later.${Off}"
        exit
    fi

    export template_folder="$project_root"/"$folder_tools"/lib/new-episode_template/

    # Folder
    echo "${Blue}==> ${Yellow} Generating Folders: ${Off} /new-ep"$episodenumber"_Title-Here and /new-ep"$episodenumber"_Title-Here/lang and /new-ep"$episodenumber"_Title-Here/lang/en "
    mkdir -p "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en
    cd "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here

    # Cover
    echo "${Blue}==> ${Yellow} Generating Cover: ${Off} E$episodenumber "
    cp "$template_folder"/cover.kra "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/E"$episodenumber".kra
    cp "$template_folder"/cover.svg "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/E"$episodenumber".svg
    sed -i 's/!XX/'$episodenumber'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/E"$episodenumber".svg
    svgfiles=E"$episodenumber".svg
    sed -i 's/_EYY.png/'_E"$episodenumber".png'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/"$svgfiles"

    # Header
    echo "${Blue}==> ${Yellow} Generating Header: ${Off}" E"$episodenumber"P00
    cp "$template_folder"/header.kra "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/E"$episodenumber"P00.kra
    cp "$template_folder"/header.svg "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/E"$episodenumber"P00.svg
    sed -i 's/!XX/'$episodenumber'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/E"$episodenumber"P00.svg
    svgfiles=E"$episodenumber"P00.svg
    sed -i 's/EYYP00/'E"$episodenumber"P00'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/"$svgfiles"

    # Pages
    count=1
    while [ $count -le $pagemax ]; do
      echo "${Blue}==> ${Yellow} Generating Pages: ${Off}" E"$episodenumber"P"$(printf %02d $count)"
      cp "$template_folder"/page.kra "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/E"$episodenumber"P"$(printf %02d $count)".kra
      cp "$template_folder"/page.svg "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/E"$episodenumber"P"$(printf %02d $count)".svg
      svgfiles=E"$episodenumber"P"$(printf %02d $count)".svg
      sed -i 's/EYYPXX/'E"$episodenumber"P"$(printf %02d $count)"'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/"$svgfiles"
      sed -i 's/!XX/'"$(printf %02d $count)"'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/"$svgfiles"
      ((count++))
    done

    # Credits
    echo "${Blue}==> ${Yellow} Generating Credits: ${Off}" E"$episodenumber"P"$(printf %02d $count)"
    cp "$template_folder"/credit.kra "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/E"$episodenumber"P"$(printf %02d $count)".kra
    cp "$template_folder"/credit.svg "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/E"$episodenumber"P"$(printf %02d $count)".svg
    svgfiles=E"$episodenumber"P"$(printf %02d $count)".svg
    sed -i 's/EYYPXX/'E"$episodenumber"P"$(printf %02d $count)"'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/lang/en/"$svgfiles"

    # Other ( README, font, script )
    echo "${Blue}==> ${Yellow} Generating others: ${Off} README.md"
    cp "$template_folder"/README.md "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/README.md
    sed -i 's/YY/'$episodenumber'/g' "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/README.md

    echo "${Blue}==> ${Yellow} Generating others: ${Off} info.yaml"
    version_inkscape="$(inkscape --version | sed -rn "s/^inkscape ([0-9.]*) .*$/\1/ip" || echo "???")"
    version_krita="$(krita --version | sed -rn "s/^krita ([0-9.]*)$/\1/ip" || echo "???")"
    version_gmic="$(gmic --version 2>/dev/null | sed -rn "s/^.*Version ([0-9.]*)[^0-9.].*$/\1/ip" || echo "???")"
    sed "
        s/%ID%/$(printf %d $episodenumber)/g
        s/%SERIAL%/$(( $episodenumber - 1 ))/g
        s/%VERSION_KRITA%/$version_krita/g
        s/%VERSION_INKSCAPE%/$version_inkscape/g
        s/%VERSION_GMIC%/$version_gmic/g
    " "$template_folder"/info.yaml > "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/info.yaml


    echo ""
    echo "${Blue}==> ${Yellow} Start initial render:"
    "$script_absolute_path"/renderfarm.sh "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/

    echo "${Blue}==> ${Yellow} Cleaning initial folder:"
    cd "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/"$folder_backup"
    rm -rf *.kra
    cd "$project_root"/"$folder_webcomics"/new-ep"$episodenumber"_Title-Here/"$folder_wip"
    rm -rf *.jpg
    cd ..

    ls -a | tee -a "$log_file"

    echo "${Green} Done. Episode created in the 'New' folder. Rename it when you have a title. ${Off}"

}

# Script
function_decorative_header
function_menu
function_create_new_episode

# Footer
function_load_source_lib "$script_absolute_path"/lib/footer.sh

if [ "$1" = "--prompt" ]; then
# Task is executed inside a terminal window (pop-up)
# This line prevent terminal windows to be auto-closed
# and necessary to read log later, or to reload the script
 echo "${Purple}  Press [Enter] to exit or [r](then Enter) to reload $script_filename${Off}"
 read -p "?" ANSWER
 if [ "$ANSWER" = "r" ]; then
   "$project_root"/"$folder_tools"/"$script_filename" --prompt
 fi
fi
