#!/bin/bash

#: Title       : Pepper&Carrot SVG Walker; svg-walker.sh
#: Author      : David REVOY < info@davidrevoy.com >,
#: License     : GPL

# a Bash script to visit all SVG on the project.
# Gateway for massive batch maintainance.

# Information
export script_title="SVG Walker"
export script_version="0.1a"
export script_filename="svg-walker.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config
function_dependency_check inkscape convert

# Script
function_decorative_header

# Carrot Ascii-art badge
echo " ${Yellow}${PurpleBG}                            ${Off}"
echo " ${Yellow}${PurpleBG}    ==== SVG-WALKER ====>   ${Off}"
echo " ${Yellow}${PurpleBG}                            ${Off}"


function_decorative_title "BROWSE ALL SVGs"
# =========================================
fileconvertedcount=0

  cd "$project_root"/"$folder_webcomics"
  # Loop all episodes
  for folder_episode in ep*; do
    echo ""
    echo "${Blue}[folder]${Off} $folder_episode"
    export path_episode="$project_root"/"$folder_webcomics"/"$folder_episode"
    # only episode folders
    if [ ! -d "$path_episode" ]; then
      break
    fi
      
      # Re-render the [low-res] from the [hi-res]
      cd "$path_episode"/hi-res
		for hiresjpgfile in $(find . -name '*.jpg'); do
        fileconvertedcount=$((fileconvertedcount+1))
        jpgfullpath=$(readlink -m $hiresjpgfile)
        if echo "$hiresjpgfile" | grep -q 'gfx' ; then
          # rule for gfx
          convert "$jpgfullpath" -resize 1280x\> -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 89% "$path_episode"/low-res/"$hiresjpgfile"
          echo "* [$fileconvertedcount] $hiresjpgfile => ${Purple}(gfx skip cropping)${Off}"
        elif echo "$hiresjpgfile" | grep -q 'P[0-9][0-9]' ; then
          # we get a page
          convert "$jpgfullpath" -chop 0x70 -resize 1280x\> -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 89% "$path_episode"/low-res/"$hiresjpgfile"
          echo "* [$fileconvertedcount] $hiresjpgfile => ${Yellow}done${Off}"
        else
          # we get a cover
          convert "$jpgfullpath" -resize 1280x\> -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 89% "$path_episode"/low-res/"$hiresjpgfile"
          echo "* [$fileconvertedcount] $hiresjpgfile => ${Blue}(cover skip cropping)${Off}"
        fi
      done
      
      # Filename for single-page target 
		cd "$path_episode"
		for krafile in *.kra; do
			if [ "$krafile" = E??.kra ]; then
				singlepagefile=$(echo $krafile|sed 's/\(.*\)\..\+/\1/')"XXL.jpg"
				singlepagefileby=Pepper-and-Carrot_by-David-Revoy_"$singlepagefile"
			fi
		done
      
      # Compute single-page montage for each lang 
      cd "$path_episode"/lang
      for langdir in *; do
			if [ -d "${langdir}" ]; then
			 echo "* [$langdir] singlepage"
			 montage -mode concatenate -tile 1x "$path_episode"/low-res/"$langdir"*P??.jpg -colorspace sRGB -quality 89% "$path_episode"/low-res/single-page/"$langdir"_"$singlepagefileby"
			fi
      done
  done

echo "${Blue} * TOTAL PNGs : $fileconvertedcount ${Off}"

# Footer library
function_load_source_lib "$script_absolute_path"/lib/footer.sh

if [ "$1" = "--prompt" ]; then
# Task is executed inside a terminal window (pop-up)
# This line prevent terminal windows to be auto-closed
# and necessary to read log later, or to reload the script
 echo "${Purple}  Press [Enter] to exit or ${White}${BlueBG}[r]${Off} to reload. ${Off}"
 read -p "?" ANSWER
 if [ "$ANSWER" = "r" ]; then
   "$project_root"/"$folder_tools"/"$script_filename" --prompt
 fi
fi
