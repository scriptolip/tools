#!/bin/bash
#: Title       : Pepper&Carrot Renderfarm
#: Author      : David REVOY < info@davidrevoy.com >, Mjtalkiewicz (aka Player_2)
#: License     : GPLv3 or higher

# Render episode of Pepper&Carrot.
# Usage: ./renderfarm.sh /path/to/folder/to/be/rendered

# Information
export script_title="Renderfarm"
export script_version="7.0a"
export script_filename="renderfarm.sh"

# Header library
export script_absolute_path="`dirname \"$0\"`"
function_load_source_lib () {
    if [ -f "$1" ]; then
        source "$1"
      else
        echo "${Red}* Error:${Off} ${Red}$1${Off} not found."
        exit
    fi
}

function_load_source_lib "$script_absolute_path"/lib/header.sh

function_load_config

function_dependency_check convert git inkscape md5sum montage notify-send parallel optipng

# Script
function_decorative_header

# Renderfarm specific variable
###############################

# Read the main path sent by user
export input_path=$1
if ! [ -d "$input_path" ]; then
  echo "Warning: not a valid folder."
  echo "Use it this way:  ./renderfarm.sh /path/to/folder/to/be/rendered"
  exit
fi
export base_folder_name="$(basename $input_path)"

# Modules ( 1=activate, 0=ndesactivate ):
export singlepage_generation=1

# Memory token
export gfx_need_regen=0
export svg_need_commit=0

# Check the name of our project folder to activate special module.
function_check_project () {

  cd "$input_path"
  echo "${Yellow}[SETUP]${Off}"
  
  if echo "$base_folder_name" | grep -q 'New'; then
    echo "New episode" | tee -a "$log_file"
    singlepage_generation=1
    cropping_pages=1

  else 
    echo "Normal mode" | tee -a "$log_file"
    singlepage_generation=1
    cropping_pages=1
    
    # update repository peppercarrot/webcomics
    function_git_update "$project_root"/"$folder_webcomics"
  fi
  
  echo "==> $input_path" | tee -a "$log_file"
  
  echo "${Yellow} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"
}

# Check the folder structure and (re)create it if necessary
function_check_folders () {
  cd "$input_path"
  
  if [ -d "$input_path/$folder_cache" ]; then
    echo " * $folder_cache found" 
  else
    echo "${Green} * creating folder: $folder_cache ${Off}"
    mkdir -p "$input_path"/"$folder_cache"
  fi

  if [ -d "$input_path/$folder_lowres" ]; then
    echo " * $folder_lowres found" 
  else
    echo "${Green} * creating folder: $folder_lowres/$folder_gfxonly ${Off}"
    mkdir -p "$input_path"/"$folder_lowres"/"$folder_gfxonly"
  fi

  if [ -d "$input_path/$folder_hires" ]; then
    echo " * $folder_hires found" 
  else
    echo "${Green} * creating folder: $folder_hires/$folder_gfxonly ${Off}"
    mkdir -p "$input_path"/"$folder_hires"/"$folder_gfxonly"
  fi
  
  if [ -d "$input_path"/"$folder_hires"/"$folder_gfxonly"/"$folder_lossless" ]; then
    echo " * $folder_hires"/"$folder_gfxonly"/"$folder_lossless found" 
  else
    echo "${Green} * creating folder: $folder_hires"/"$folder_gfxonly"/"$folder_lossless ${Off}"
    mkdir -p "$input_path"/"$folder_hires"/"$folder_gfxonly"/"$folder_lossless"
  fi
  
  if [ -d "$input_path"/"$folder_hires"/"$folder_txtonly" ]; then
    echo " * $folder_txtonly found" 
  else
    echo "${Green} * creating folder: $folder_hires/$folder_txtonly ${Off}"
    mkdir -p "$input_path"/"$folder_hires"/"$folder_txtonly"
  fi

  if [ -d "$input_path"/"$folder_hires"/"$folder_html" ]; then
    echo " * $folder_html found" 
  else
    echo "${Green} * creating folder: $folder_hires/$folder_html ${Off}"
    mkdir -p "$input_path"/"$folder_hires"/"$folder_html"
  fi

  if [ -d "$input_path/$folder_backup" ]; then
    echo " * $folder_backup found" 
  else
    echo "${Green} * creating folder: $folder_backup ${Off}"
    mkdir -p "$input_path"/"$folder_backup"
  fi
  
  if [ -d "$input_path/$folder_wip" ]; then
    echo " * $folder_wip found" 
  else
    echo "${Green} * creating folder: $folder_wip ${Off}"
    mkdir -p "$input_path"/"$folder_wip"
  fi
  
  if [ $singlepage_generation = 1 ]; then
    if [ -d "$input_path/$folder_lowres/$folder_singlepage" ]; then
      echo " * $folder_singlepage found" 
    else
      echo "${Green} * creating folder: $folder_lowres/$folder_singlepage ${Off}"
      mkdir -p "$input_path"/"$folder_lowres"/"$folder_singlepage"
    fi
  fi
  
  if [ -d "$input_path/$folder_lang" ]; then
    echo " * $folder_lang found" 
  else
    echo "${Green} * creating folder: $folder_lang ${Off}"
    mkdir -p "$input_path"/"$folder_lang"
  fi

}

# Main fonction for rendering Krita files (comic pages artworks)
function_render_kra() {
  krafile=$1
  cd "$input_path"
  txtfile=$(echo $krafile|sed 's/\(.*\)\..\+/\1/')".txt"
  pngfile=$(echo $krafile|sed 's/\(.*\)\..\+/\1/')".png"
  jpgfile=$(echo $krafile|sed 's/\(.*\)\..\+/\1/')".jpg"
  svgfile=$(echo $krafile|sed 's/\(.*\)\..\+/\1/')".svg"
  kra_tmpfolder=$(echo $krafile|sed 's/\(.*\)\..\+/\1/')""
  jpgfileversionning=$(echo $krafile|sed 's/\(.*\)\..\+/\1/')_$version".jpg"
  rendermefile=$(echo $krafile|sed 's/\(.*\)\..\+/\1/')"-renderme.txt"
  pngfileby="$project_name"_"$project_attribution"_"$pngfile"
  jpgfileby="$project_name"_"$project_attribution"_"$jpgfile"
  gfxpngfileby=gfx_"$pngfileby"
  gfxjpgfileby=gfx_"$jpgfileby"
  
  # Read the checksum of *.kra file
  md5read="`md5sum $krafile`"
  
  # Avoid grep to fail if no file found
  if [ -f "$input_path"/"$folder_cache"/"$txtfile" ]; then
    true
  else
    touch "$input_path"/"$folder_cache"/"$txtfile"
  fi
  
  # Compare if actual *.kra checksum is similar to the previous one recorded on txtfile
  if grep -q "$md5read" "$input_path"/"$folder_cache"/"$txtfile"; then
    echo " ==> [kra] $krafile file is up-to-date."
  else
    echo "${Green} ==> [kra] $krafile is new or modified, rendered. ${Off}"

    # Update the cache with a new version
    md5sum "$krafile" > "$input_path"/"$folder_cache"/"$txtfile"

    # Extract the PNG hi-res directly from *.kra
    # Create a tmp folder for unzipping
    mkdir -p /tmp/"$kra_tmpfolder"
    # Unzipping the target file
    unzip -j "$input_path"/"$krafile" "mergedimage.png" -d /tmp/"$kra_tmpfolder"
    # Make a PNG without Alpha, compressed to max, and a sRGB colorspace.
    convert /tmp/"$kra_tmpfolder"/"mergedimage.png" -colorspace sRGB -background white -alpha remove -define png:compression-strategy=3 -define png:color-type=2 -define png:compression-level=9  "$input_path"/"$folder_cache"/"$gfxpngfileby"
    # Job done, remove the tmp folder.
    rm -rf /tmp/"$kra_tmpfolder"

    # Processing a special export untranslated to low-res/EXX.jpg for the cover/thumbnail
    if [ "$krafile" = E??.kra ]; then
      convert "$input_path"/"$folder_cache"/"$gfxpngfileby" -resize "$lowres_jpg_resize" -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 92% "$input_path"/"$folder_lowres"/"$jpgfileby"
      # In case of a cover artwork upgraded; request a regeneration of title
      rendermefile=$(echo $krafile|sed 's/\(.*\)\..\+/\1/')"P00-renderme.txt"
    fi

    # Generate WIP jpg : full res, JPG, 95%, no lang
    convert "$input_path"/"$folder_cache"/"$gfxpngfileby" -colorspace sRGB -background white -alpha remove -quality 95% "$input_path"/"$folder_wip"/"$jpgfileversionning"

    # Scale down resolution if necessary to match SVG size:
    convert "$input_path"/"$folder_cache"/"$gfxpngfileby" -resize 2481x\> "$input_path"/"$folder_cache"/"$gfxpngfileby"

    # Update Hires gfx-only folder
    convert -units PixelsPerInch -strip -interlace Plane "$input_path"/"$folder_cache"/"$gfxpngfileby" -density 300 -colorspace sRGB -quality 95% "$input_path"/"$folder_hires"/"$folder_gfxonly"/"$gfxjpgfileby"

    # Update Hires lossless gfx-only png
    convert -units PixelsPerInch -strip -interlace Plane "$input_path"/"$folder_cache"/"$gfxpngfileby" -density 300 -colorspace sRGB -background white -alpha remove -define png:compression-strategy=3 -define png:color-type=2 -define png:compression-level=9 "$input_path"/"$folder_hires"/"$folder_gfxonly"/"$folder_lossless"/"$gfxpngfileby"

    if [ "$krafile" = E??.kra ]; then
        # Do not export the cover PNG in lang
        true
    else
        # Generate low-res *.png in lang
        if [ -f "$input_path"/"$folder_lang"/"alpha" ]; then
            # new episode: compress max for git history WIP:
            convert "$input_path"/"$folder_cache"/"$gfxpngfileby" -resize 694x981 -colors 64 "$input_path"/"$folder_lang"/"$gfxpngfileby"
            optipng -o7 -quiet "$input_path"/"$folder_lang"/"$gfxpngfileby"
        else
            # render normal resolution:
            convert "$input_path"/"$folder_cache"/"$gfxpngfileby" -resize 990x1398 -colorspace sRGB -quality 92% "$input_path"/"$folder_lang"/"$gfxpngfileby"
            optipng -o1 -quiet "$input_path"/"$folder_lang"/"$gfxpngfileby"
        fi
    fi

    # Generate low-res gfx_file.jpg in low-res/gfx-only
    convert "$input_path"/"$folder_cache"/"$gfxpngfileby" -resize "$lowres_jpg_resize" -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 92% "$input_path"/"$folder_lowres"/"$folder_gfxonly"/"$gfxjpgfileby"

    # Copy a backup of kra
    cp "$input_path"/"$krafile" "$input_path"/"$folder_backup"/"$version"_"$krafile"

    cd "$input_path"/"$folder_lang"/

    for langdir in */; do

      # Clean folder, remove trailing / character
      langdir="${langdir%%?}"

        # Create folder
        mkdir -p "$input_path"/"$folder_cache"/"$langdir"

        # Position cursor inside the current folder
        cd "$input_path"/"$folder_cache"/"$langdir"/

        # Create a dummy file token to indicate singlepage to render
        touch "$input_path"/"$folder_cache"/"$langdir"/need_render.txt

        # Send a renderme token to let know update_lang_work he has rendering work to do
        touch "$input_path"/"$folder_cache"/"$langdir"/"$rendermefile"

    done
  fi
}

# Additional fonction for rendering and handling the gif animation
function_render_gif() {
  giffile=$1
  pngfile=$(echo $giffile|sed 's/\(.*\)\..\+/\1/')".png"

  # Compare if gif file changed
  if diff "$input_path"/"$giffile" "$input_path"/"$folder_cache"/"$giffile" &>/dev/null ; then
    echo " ==> [gif] $giffile is up-to-date."
  else
    echo " ${Green}==> [gif] $giffile new or modified: rendered. ${Off}"

    # Update cache
    cp "$input_path"/"$giffile" "$input_path"/"$folder_cache"/"$giffile"
    
    # Ensure to reset on folder_lang on the start of the loop
    cd "$input_path"/"$folder_lang"/

    for langdir in */;
    do
      # Clean folder, remove trailing / character
      langdir="${langdir%%?}"
      
      # Prevent missing lang folder at a first run
      mkdir -p "$input_path"/"$folder_cache"/"$langdir"

      # Spread the Gif as it is in all the pages (gifs have no translations)
      cp "$input_path"/"$folder_cache"/"$giffile"  "$input_path"/"$folder_lowres"/"$langdir"_"$project_name"_"$project_attribution"_"$giffile"
      cp "$input_path"/"$folder_cache"/"$giffile"  "$input_path"/"$folder_lowres"/"$folder_gfxonly"/gfx_"$project_name"_"$project_attribution"_"$giffile"
      cp "$input_path"/"$folder_cache"/"$giffile"  "$input_path"/"$folder_hires"/"$langdir"_"$project_name"_"$project_attribution"_"$giffile"
      cp "$input_path"/"$folder_cache"/"$giffile"  "$input_path"/"$folder_hires"/"$folder_gfxonly"/gfx_"$project_name"_"$project_attribution"_"$giffile"

      # New strategy for static image to the Gif panel alternative ( for print, for single page )
      # Does we have an alternative PNG static file next to the Gif ?
      if [ -f "$input_path"/"$pngfile" ]; then
        # Yes. We copy it.
        cp "$input_path"/"$pngfile" "$input_path"/"$folder_lang"/gfx_"$project_name"_"$project_attribution"_"$pngfile"
        # copy it also to lossless 
        cp "$input_path"/"$pngfile" "$input_path"/"$folder_hires"/"$folder_gfxonly"/"$folder_lossless"/gfx_"$project_name"_"$project_attribution"_"$pngfile"
      else
        # No. alternative PNG files were found, we need to auto-generate one ( using the first frame of the gif-anim).
        gifframe1="$input_path"/"$folder_cache"/"$giffile"[0]
        convert "$gifframe1" -resize "$lowres_jpg_resize" -colorspace sRGB -quality 92% "$input_path"/"$folder_lang"/gfx_"$project_name"_"$project_attribution"_"$pngfile"
      fi
      
      # Create a dummy file token to indicate what lang where changed
      touch "$input_path"/"$folder_cache"/"$langdir"/need_render.txt
      
    done
  fi
}

function_parallel_render_gfx () {
  # Method to update change in graphical file
  # Trying to be smart and consume the less power, but more disk space.
  # Only file changed are reprocessed thanks to the folder cache

  echo "${Yellow} [GFX]${Off}"
  echo "${Yellow} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"
  
  # Project should contain *.kra artworks anyway
  cd "$input_path"
  export -f function_render_kra
  ls -1 *.kra | parallel function_render_kra "{}"
  
  # Project might contain *.gif animation
  cd "$input_path"
  getamountofgif=`ls -1 *.gif 2>/dev/null | wc -l`
  if [ $getamountofgif != 0 ]; then 
    export -f function_render_gif
    ls -1 *.gif | parallel function_render_gif "{}"
  fi

}

# Main fonction for rendering SVG
function_render_svg() {
  cd "$input_path"/"$folder_lang"/
  langdir=$1
  
  # Clean lang folder name, remove trailing / character
  langdir="${langdir%%?}"

  # Ensure to reset on folder_lang on the start of the loop
  cd "$input_path"/"$folder_lang"/

    # Check if the target folder exist in case of a new lang
    if [ -d "$input_path/$folder_cache/$langdir" ]; then
      true
    else
      # create the lang dir, and a sub lang dir for later montage
      mkdir -p "$input_path"/"$folder_cache"/"$langdir"/"$langdir"
      
      # If a new lang folder exist, we need to also copy it's *.gif to low-res
      cd "$input_path"
      getamountofgif=`ls -1 *.gif 2>/dev/null | wc -l`
      if [ $getamountofgif != 0 ]; then 
        for giffile in *.gif; do
          cp "$input_path"/"$folder_cache"/"$giffile"  "$input_path"/"$folder_lowres"/"$langdir"_"$project_name"_"$project_attribution"_"$giffile"
          cp "$input_path"/"$folder_cache"/"$giffile"  "$input_path"/"$folder_lowres"/"$folder_gfxonly"/gfx_"$project_name"_"$project_attribution"_"$giffile"
          cp "$input_path"/"$folder_cache"/"$giffile"  "$input_path"/"$folder_hires"/"$langdir"_"$project_name"_"$project_attribution"_"$giffile"
          cp "$input_path"/"$folder_cache"/"$giffile"  "$input_path"/"$folder_hires"/"$folder_gfxonly"/gfx_"$project_name"_"$project_attribution"_"$giffile"
        done
      fi
      
    fi
    
    # Position cursor inside the current cache/lang
    cd "$input_path"/"$folder_lang"/"$langdir"/
    
    # create the txtonly subfolder to store modified txtonly SVG
    mkdir -p "$input_path"/"$folder_cache"/"$langdir"/"$folder_txtonly"

    # Get the directory name of the current episode
    episode_directory=`basename "$input_path"`

    # New loop : we process the SVG of the current lang dir
    for svgfile in *.svg; do
      pngfile=$(echo $svgfile|sed 's/\(.*\)\..\+/\1/')".png"
      jpgfile=$(echo $svgfile|sed 's/\(.*\)\..\+/\1/')".jpg"
      rendermefile=$(echo $svgfile|sed 's/\(.*\)\..\+/\1/')"-renderme.txt"
      pngfileby="$project_name"_"$project_attribution"_"$pngfile"
      jpgfileby="$project_name"_"$project_attribution"_"$jpgfile"
      gfxpngfileby=gfx_"$pngfileby"

      if [ "$svgfile" = E??P??.svg*.svg ]; then
        echo "${Red} [ERROR] Inscape autosave file detected: $input_path/$folder_lang/$langdir/$svgfile ${Off}"
        echo "${Red}         Skipping rendering for this SVG, please delete manually this file and rerun the script later to remove this message. ${Off}"
        continue
      fi
      
      # Compare if langage folder changed compare to the version we cached in cache/lang/lang
      if diff "$input_path"/"$folder_lang"/"$langdir"/"$svgfile" "$input_path"/"$folder_cache"/"$langdir"/"$svgfile" &>/dev/null ; then
       true
      else
        touch "$input_path"/"$folder_cache"/"$langdir"/"$rendermefile"
      fi
      
      # Check if there is not a ready made renderme token ready
      if [ ! -f "$input_path"/"$folder_cache"/"$langdir"/"$rendermefile" ]; then
        true
      else

        echo "${Green} ==> [$langdir] $svgfile is new or modified ${Off}"
        
        # Sanify routine check for SVG:
        if grep -q 'xlink:href="../gfx_' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"; then
          # everything is ok, no news = good news.
          true
        else
          # 1. Specific fix for Redmond path with backslash in path:
          if grep -q 'xlink:href=".*.\\gfx_' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"; then
            # Show a visible feedback alert in red:
            echo "${Green} ==> [autofix attempt] Redmond(1): $input_path/$folder_lang/$langdir/$svgfile : commit changes later please ${Off}"
            # Display the line of the issue in red:
            echo "${Red}      (-)"
            grep 'xlink:href="' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"
            echo "${Off}"
            
            # Replace pattern with sed twice in case of two images embed, rare but might happen
            sed -i 's/xlink:href=".*.\\gfx/xlink:href="..\/gfx/g' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"
            sed -i 's/xlink:href=".*.\\gfx/xlink:href="..\/gfx/g' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"
            
            # Display the line of the fixed result in green:
            echo "${Green}      (+)"
            grep 'xlink:href="' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"
            echo "${Off}"
          fi
          # 2. Specific fix for Inkscape rewriting the path when user move the file around:
          if grep -q 'xlink:href=".*./gfx_' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"; then
            # Show a visible feedback alert in red:
            echo "${Green} ==> [autofix attempt] Path moved around(2): $input_path/$folder_lang/$langdir/$svgfile : commit changes later please ${Off}"
            # Display the line of the issue in red:
            echo "${Red}      (-)"
            grep 'xlink:href="' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"
            echo "${Off}"
            
            # Replace pattern with sed twice in case of two images embed, rare but might happen
            sed -i 's/xlink:href=".*.\/gfx/xlink:href="..\/gfx/g' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"
            sed -i 's/xlink:href=".*.\/gfx/xlink:href="..\/gfx/g' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"
            
            # Display the line of the fixed result in green:
            echo "${Green}      (+)"
            grep 'xlink:href="' "$input_path"/"$folder_lang"/"$langdir"/"$svgfile"
            echo "${Off}"
          fi
        fi

			# [hi-res] (JPG)
			# ==============
			echo "[hi-res]"
			# Update cache
			cp "$input_path"/"$folder_lang"/"$langdir"/"$svgfile" "$input_path"/"$folder_cache"/"$langdir"/"$svgfile"
			# Render the lang with Inkscape to PNG
			inkscape "$input_path"/"$folder_cache"/"$langdir"/"$svgfile" -o "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$pngfileby"
			# Sanify the PNG
			convert -strip -interlace Plane -colorspace sRGB -units PixelsPerInch "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$pngfileby" -density 300 -colorspace sRGB -background white -alpha remove -define png:compression-level=1 "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$pngfileby"
			# Convert the PNG to final hi-res JPG
			convert -strip -interlace Plane -colorspace sRGB -units PixelsPerInch "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$pngfileby" -density 300 -colorspace sRGB -quality 95% "$input_path"/"$folder_hires"/"$langdir"_"$jpgfileby"

			# [txtonly] (PNG)
			# ===============
			echo "[text-only]"
			# Update cache
			cp "$input_path"/"$folder_lang"/"$langdir"/"$svgfile" "$input_path"/"$folder_cache"/"$langdir"/"$folder_txtonly"/"$svgfile"
			# Create a full alpha PNG
			convert -size 1x1 xc:none "$input_path"/"$folder_cache"/"$langdir"/"$gfxpngfileby"
			# Render the lang with inkscape against the empty gfx for background
			inkscape "$input_path"/"$folder_cache"/"$langdir"/"$folder_txtonly"/"$svgfile" -o "$input_path"/"$folder_cache"/"$langdir"/"$folder_txtonly"/"$langdir"_"$pngfileby"
			# Sanify the PNG and draw point on the corner for consistent geometry in software cropping alpha around content
			convert "$input_path"/"$folder_cache"/"$langdir"/"$folder_txtonly"/"$langdir"_"$pngfileby" -units PixelsPerInch -density 300 -colorspace sRGB -define png:compression-level=9 -background White -alpha Background -fill '#ffffff11' -draw 'point 0,0 point 2480,0 point 0,3502 point 2480,3502' "$input_path"/"$folder_hires"/"$folder_txtonly"/"$langdir"_"$pngfileby"

        # [low-res] (JPG)
        # ===============
        # crop the top of pages for fluid scrolling and no big padding of page jumps
        convert "$input_path"/"$folder_hires"/"$langdir"_"$jpgfileby" -chop 0x70 -resize "$lowres_jpg_resize" -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 89% "$input_path"/"$folder_lowres"/"$langdir"_"$jpgfileby"

        # Cover generation
        # ================
        if [ "$svgfile" = E??P00.svg ]; then

            # Regenerate all epXX_xxxxx/hi-res/titles.json
            cd "$project_root"/"$folder_webcomics"/0_transcripts/
            ./collect_titles.py >/dev/null 2>&1
            cd "$input_path"/"$folder_lang"/"$langdir"/

            covername=$(echo $svgfile|sed 's/\(.*\)\..\+/\1/')""
            covername="${covername::-3}"
            pngcoverfile=$covername".png"
            jpgcoverfile=$covername".jpg"
            pngcoverfileby="$project_name"_"$project_attribution"_"$pngcoverfile"
            jpgcoverfileby="$project_name"_"$project_attribution"_"$jpgcoverfile"

            # [txtonly]: Render the cover with utils/covertext.sh:
            bash "$project_root"/"$folder_tools"/"utils/covertext.sh" "$episode_directory" "$langdir" "$input_path"/"$folder_hires"/"$folder_txtonly"/"$langdir"_"$pngcoverfileby"
            # [Hi-res]: Composite
            composite "$input_path"/"$folder_hires"/"$folder_txtonly"/"$langdir"_"$pngcoverfileby" "$input_path"/"$folder_cache"/gfx_"$pngcoverfileby" -gravity South -geometry +0-10 "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$pngcoverfileby"
            convert -strip -interlace Plane -colorspace sRGB -units PixelsPerInch "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$pngcoverfileby" -density 300 -colorspace sRGB -quality 95% "$input_path"/"$folder_hires"/"$langdir"_"$jpgcoverfileby"
            # [low-res]: export
            convert "$input_path"/"$folder_hires"/"$langdir"_"$jpgcoverfileby" -resize 640x -unsharp 0x0.50+0.50+0 -colorspace sRGB -quality 75% "$input_path"/"$folder_lowres"/"$langdir"_"$jpgcoverfileby"
        fi

        # Create a token to indicate lang had an update (to regenerate later single-page)
        touch "$input_path"/"$folder_cache"/"$langdir"/need_render.txt

      fi
    done
}

function_parallel_render_lang() { 
  echo ""
  echo "${Yellow} [LANG] ${Off}"
  echo "${Yellow} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"

  export -f function_render_svg
  cd "$input_path"/"$folder_lang"/ && ls -1d */ | parallel function_render_svg "{}"
}

# Main fonction for rendering Transcript
function_render_transcript() {
  cd "$input_path"/"$folder_lang"/
  langdir=$1
  # Clean lang folder name, remove trailing / character
  langdir="${langdir%%?}"

  episodefolder=$(basename "$input_path")
  episodeprefix=${episodefolder:0:4}
  transcriptfile="$episodeprefix"_"$langdir"_transcript.md

  # Ensure to reset on folder_lang on the start of the loop
  cd "$input_path"/"$folder_lang"/

    # Check if the target folder exist in case of a new lang
    if [ -d "$input_path/$folder_cache/$langdir" ]; then
      true
    else
      # create the lang dir, and a sub lang dir for later montage
      mkdir -p "$input_path"/"$folder_cache"/"$langdir"
    fi

    # Bother a try only if a markdown file exist in the directory
    if [ -f "$input_path"/"$folder_lang"/"$langdir"/"$transcriptfile" ]; then

        # Position cursor inside the current cache/lang
        cd "$input_path"/"$folder_lang"/"$langdir"/

        # Compare if langage folder changed compare to the version we cached in cache/lang/lang
        if diff "$input_path"/"$folder_lang"/"$langdir"/"$transcriptfile" "$input_path"/"$folder_cache"/"$langdir"/"$transcriptfile" &>/dev/null ; then
        true
        else
            echo "${Green} ==> [$langdir] $transcriptfile is new or modified ${Off}"

            # render the transcript
            cd "$project_root"/"$folder_webcomics"/0_transcripts/
            ./extract_to_html.py "$episodefolder" "$langdir"

            # update the transcript cached
            cp -R "$input_path"/"$folder_lang"/"$langdir"/"$transcriptfile" "$input_path"/"$folder_cache"/"$langdir"/"$transcriptfile"
        fi

    fi
}

function_parallel_render_transcript() { 
  echo ""
  echo "${Yellow} [TRANSCRIPT] ${Off}"
  echo "${Yellow} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"

  export -f function_render_transcript
  cd "$input_path"/"$folder_lang"/ && ls -1d */ | parallel function_render_transcript "{}"
}

# A function to create a long strip for a vertical montage of all pages
function_create_montage_singlepage() {

  cd "$input_path"/"$folder_lang"/
  langdir=$1

  # Clean folder, remove trailing / character
  langdir="${langdir%%?}"

  # Repositioning to the main folder
  cd "$input_path"

  # Create the name, relative to the cover kra name.
  for krafile in *.kra; do
    if [ "$krafile" = E??.kra ]; then
      singlepagefile=$(echo $krafile|sed 's/\(.*\)\..\+/\1/')"XXL.jpg"
      singlepagefileby="$project_name"_"$project_attribution"_"$singlepagefile"
    fi
  done

  # Create a subcache for our work
  mkdir -p "$input_path"/"$folder_cache"/"$langdir"/montage/

  # Repositioning to the cache/lang folder
  cd "$input_path"/"$folder_cache"/"$langdir"/

  # if dummy file token exist in lang folder cached, we need to re-render then clean dummy.
  if [ -f "$input_path"/"$folder_cache"/"$langdir"/need_render.txt ]; then
    echo "${Green} ==> [$langdir] montage rendered${Off}"

    # If project get updated *.gif , copy before generating the single page, but as static PNG to be catched by the montage wild mask *.png loop
    cd "$input_path"
    getamountofgif=`ls -1 *.gif 2>/dev/null | wc -l`

    if [ $getamountofgif != 0 ]; then 

      for giffile in *.gif; do
      pngfile=$(echo $giffile|sed 's/\(.*\)\..\+/\1/')".png"
      jpgfile=$(echo $giffile|sed 's/\(.*\)\..\+/\1/')".jpg"
      pngfileby="$project_name"_"$project_attribution"_"$pngfile"
      jpgfileby="$project_name"_"$project_attribution"_"$jpgfile"
      # New strategy for static image to the Gif panel alternative ( for print, for single page )

        # Does we have an alternative static file next to the Gif ?
        if [ -f "$input_path"/"$pngfile" ]; then
          # Yes. We copy it.
          cp "$input_path"/"$pngfile" "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$pngfileby"
        else
          # No. Alternative static files not found, we need to generate one ( using the first frame of the gif-anim).
          gifframe1="$input_path"/"$folder_cache"/"$giffile"[0]
          convert "$gifframe1" -bordercolor white -border 0x20 -colorspace sRGB -define png:color-type=2 "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$pngfileby"
        fi

      # convert the result to the low-res JPG format
      convert "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$pngfileby" -colorspace sRGB -quality 92% "$input_path"/"$folder_cache"/"$langdir"/montage/"$langdir"_"$jpgfileby"
      done
    fi

    # Repositioning in the hi-res folder
    cd "$input_path"/"$folder_hires"/

    # Get temporary all the hi-res JPG in montage cache for fusion
    cp "$langdir"*.jpg "$input_path"/"$folder_cache"/"$langdir"/montage/

    # Repositioning to the cache/lang folder
    cd "$input_path"/"$folder_cache"/"$langdir"/montage/

    # create the montage with imagemagick from all jpg found with a page pattern in cache folder.
    montage -mode concatenate -tile 1x *P??.jpg -colorspace sRGB -quality 92% -resize "$lowres_jpg_resize" "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$singlepagefileby"

    # copy the rendering in the final folder
    cp "$input_path"/"$folder_cache"/"$langdir"/"$langdir"_"$singlepagefileby" "$input_path"/"$folder_lowres"/"$folder_singlepage"/"$langdir"_"$singlepagefileby"

  else
   echo " ==> [$langdir] $langdir_$jpgfile is up-to-date."
  fi
}

function_parallel_render_singlepage () {
  echo ""
  echo "${Yellow} [SINGLEPAGE]${Off}"
  echo "${Yellow} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"
  
  export -f function_create_montage_singlepage
  cd "$input_path"/"$folder_lang"/ && ls -1d */ | parallel function_create_montage_singlepage "{}"
}

function_clean_cache()
{
  cd "$input_path"/"$folder_cache"/

  for langdir in */; do

    # Clean folder, remove trailing / character
    langdir="${langdir%%?}"
    
    # Repositioning to the cache/lang folder
    cd "$input_path"/"$folder_cache"/"$langdir"/
      
    # clean up files
    rm -f "$input_path"/"$folder_cache"/"$langdir"/*.png
    rm -f "$input_path"/"$folder_cache"/"$langdir"/*.jpg
    rm -f "$input_path"/"$folder_cache"/"$langdir"/*.txt
    
    # clean up folders
    rm -rf "$input_path"/"$folder_cache"/"$langdir"/montage
    rm -rf "$input_path"/"$folder_cache"/"$langdir"/"$folder_txtonly"
    
  done
}

# Execute
function_check_project
function_check_folders
function_parallel_render_gfx
function_parallel_render_lang
function_parallel_render_transcript

# Optional module to execute
if [ $singlepage_generation = 1 ]; then
  function_parallel_render_singlepage
fi

function_clean_cache

# Footer library
function_load_source_lib "$script_absolute_path"/lib/footer.sh

if [ "$2" = "--prompt" ]; then
# Task is executed inside a terminal window (pop-up)
# This line prevent terminal windows to be auto-closed
# and necessary to read log later, or to reload the script
 echo "${Purple}  Press [Enter] to exit or [r](then Enter) to reload $script_filename${Off}"
 read -p "?" ANSWER
 if [ "$ANSWER" = "r" ]; then
   "$project_root"/"$folder_tools"/"$script_filename" "$input_path" --prompt
 fi
fi
