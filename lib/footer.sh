#!/bin/bash
# footer.sh - Shared footer functions and variables for peppercarrot script
echo ""

# Mesure script speed
script_runtime_end=$(date +"%s")
diff_runtime=$(($script_runtime_end-$script_runtime_start))

# End User Interface messages
echo "Script execution time: $(($diff_runtime / 60))min $(($diff_runtime % 60))sec." | tee -a "$log_file"
# notify-send "$script_title" "Script execution time: $(($diff_runtime / 60))min $(($diff_runtime % 60))sec."

# Print terminal title windows done
printf "\033]0;%s\007\n" "$script_title"


