#!/bin/bash
# header.sh - Shared headers functions and variables for peppercarrot script

# Mesure script speed
script_runtime_start=$(date +"%s")

# Print terminal title windows in progress
printf "\033]0;%s\007\n" "*$script_title"

# Date utils
export isodate=$(date +%Y-%m-%d)
export version=$(date +%Y-%m-%d_%Hh%M)
export versiondaily=$(date +%Y%m%d)

# Colored output
export Off=$'\e[0m'
export Purple=$'\e[1;35m'
export Blue=$'\e[1;34m'
export Green=$'\e[1;32m'
export Red=$'\e[1;31m'
export Yellow=$'\e[1;33m'
export White=$'\e[1;37m'
export BlueBG=$'\e[1;44m'
export RedBG=$'\e[1;41m'
export PurpleBG=$'\e[1;45m'
export Black=$'\e[1;30m'

# Launch the script in a terminal, adapt to terminal of user.
function_run_in_terminal() {
    if ! tty &>/dev/null; then
      if which konsole &>/dev/null; then
        konsole -e "$1 --prompt"
      elif which gnome-terminal &>/dev/null; then
        gnome-terminal --command="$1 --prompt"
      elif which xfce4-terminal &>/dev/null; then
        xfce4-terminal --command="$1 --prompt"
      elif which x-terminal-emulator &>/dev/null; then
        x-terminal-emulator -e "$1 --prompt"
      elif which xterm &>/dev/null; then
        xterm -e "$1 --prompt"
      else
        echo "[Error: Terminal not found.]" >> "$log_file"
        echo "* You don't have konsole, gnome-terminal, xfce4-terminal or xterm installed." >> "$log_file"
        echo "* If you want to add your console emulator to the source-code, add a line in tools/lib/header.sh around line 40." >> "$log_file"
        echo "* Also, more console rules are welcome. Don't forget to contact Pepper&Carrot team with your setup for including it in the main code!" >> "$log_file"
      fi
    else
      "$1"
    fi
}

# Static. Print a decorative header.
function_decorative_header () {
    echo "${White}${BlueBG}    $script_title    ${Off}"
    echo "  version: $script_version  "
    echo ""
}

# Print a decorative title for $1.
function_decorative_title () {
    echo ""
    echo "${Yellow}[$1]${Off}"
    echo "${Yellow} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"
    echo "$1" >> "$log_file"
}

# Static. just a blue line.
function_decorative_split () {
    echo "${Blue} =-=-=-=-=-=--=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= ${Off}"
}

# Static. Just check "config" and load it.
function_load_config () {
    # For script at root
    if [ -f "$script_absolute_path"/config ]; then
      source "$script_absolute_path"/config
      export script_absolute_path="$project_root"/"$folder_tools"/
        # check log file
        if [ -f "$log_file" ]; then
          # start to log
          echo "" >> "$log_file"
          echo "[ $version : $script_filename starts ]" >> "$log_file"
        else
          touch "$log_file"
        fi
    # For script on subfolder like utils/covertext.sh
    elif [ -f "$script_absolute_path"/../config ]; then
      source "$script_absolute_path"/../config
      export script_absolute_path="$project_root"/"$folder_tools"/
        # check log file
        if [ -f "$log_file" ]; then
          # start to log
          echo "" >> "$log_file"
          echo "[ $version : $script_filename starts ]" >> "$log_file"
        else
          touch "$log_file"
        fi
    else
      echo "${Red}* Error: config file not found ${Off}"
      echo "* Please setup config.sample then rename it to config."
      exit
    fi
}

# Check if dependencies $1, $2 ,$3 ... exist.
function_dependency_check () {
    for arg; do
      if command -v "$arg" >/dev/null 2>&1 ; then
        true
      else
        echo "${Red}* Error:${Off} missing dependency: ${Red}$arg${Off} not found."
        echo "Fatal: missing dependency: $arg." >> "$log_file"
        exit
      fi
    done
}

# Git update (or return status) for the full path to folders in $1, $2 ,$3 ...
function_git_update () {
    for arg; do
      # Check if we have a valid .git folder
      if [ -d "$arg"/.git ]; then
        
        cd "$arg"
        export git_folder_name="$(basename $arg)"
        
        # refresh repo to get remote informations
        git remote update | tee -a "$log_file"
        
        # git tools
        git_local=$(git rev-parse @)
        git_remote=$(git rev-parse @{u})
        git_base=$(git merge-base @ @{u})
        
        # start git update smart decisions
        if [ $git_local = $git_remote ]; then
          echo " * $git_folder_name is up-to-date" | tee -a "$log_file"
            
        elif [ $git_local = $git_base ]; then
          echo "${Blue} * $git_folder_name is outdated${Off}"
          echo "${Green} ==> [git] git pull ${Off}" 
          echo "* $git_folder_name is outdated." >> "$log_file"
          git pull | tee -a "$log_file"
                    
        elif [ $git_remote = $git_base ]; then
          echo "${Purple} * $git_folder_name contains commit non pushed${Off}"
          echo "* $git_folder_name contains commit non pushed." >> "$log_file"
          echo ""
          echo "${Purple}  Press enter to continue or [q] to quit${Off}"
          read -p "?" ANSWER
          if [ "$ANSWER" = "q" ]; then
           exit
          fi
        else
          echo "${Red} * $git_folder_name error: diverging repositories${Off}"
          echo "* $git_folder_name error: diverging repositories." >> "$log_file"
          echo ""
          echo "Press [Enter] to exit"
          read -p "?" ANSWER
          exit
        fi
          
      else
        echo " ${Red}Error:${Off} $arg is not a Git repository."
        echo "* $arg is not a Git repository." >> "$log_file"
          echo ""
          echo "Press [Enter] to exit"
          read -p "?" ANSWER
          exit
      fi
    done
}

# Upload $1(local folder) to $2(remote folder) folder via ftp url setup in config
function_ftp_upload () {
    # For simulation (--delete option can be dangerous and delete all your server) use:
    # --dry-run
    lftp -c "set ftp:list-options -a;
    set xfer:use-temp-file yes;
    open '$server_full_adress';
    lcd '$1';
    cd '$2';
    mirror --use-cache \
           --delete \
           --use-pget-n=10 \
           --no-perms \
           --reverse \
           --verbose \
           --exclude-glob new*/ \
           --exclude-glob 0_test/ \
           --exclude-glob 0_archives/ \
           --exclude-glob animation/ \
           --exclude-glob backup/ \
           --exclude-glob cache/ \
           --exclude-glob /fonts/ \
           --exclude-glob .git/ \
           --exclude-glob .gitignore \
           --exclude-glob .tmp/ \
           --exclude-glob .data/ \
           --exclude-glob .0_sources/ \
           --exclude-glob wip/ \
           --exclude-glob .directory \
           --exclude-glob *.sh \
           --exclude-glob *~ \
           --exclude-glob *.kra \
           --exclude-glob *.ora \
           --exclude-glob *.blend
    " | tee -a "$log_file"
    echo "File transfert end." | tee -a "$log_file"
}
